<script>
    ;(function (exports) {
        exports.text = {$text|jsonify};
        exports.platform = {
            template: {$template|jsonify},
            currency: {$currency|json_encode},
            general: {$general|jsonify},
            shop: {$shop|jsonify},
            language: {
                iso: '{$general.languageIso}',
                iso639: '{$general.languageIso639}'
            },
            page: {$page|jsonify},
            user: {$user|jsonify},
            settings: {$settings|jsonify},
            getSetting: function (key) {
                if (typeof this.settings[key] !== 'undefined') {
                    return this.settings[key];
                }
                return null;
            }
        };

    })(window);
</script>

{*** Display all scripts added thru addScript help ***}
{foreach $head.scripts as $script}<script {foreach $script->attributes as $k => $v}{$k}="{$v}" {/foreach}></script>
{/foreach}

{*** Facebook like ***}
{if isset($settings.social_facebook) and $settings.social_facebook}
    {if strtolower($general.languageIso) eq "uk"}
        {$lang = "en_US"}
    {else}
        {$lang = $general.languageIso639|cat:"_"|cat:$general.languageIso}
    {/if}

<div id="fb-root"></div>
<script id="facebook-jssdk" src="//connect.facebook.net/{$lang}/sdk.js#xfbml=1{if !empty($settings.fb_appId)}&amp;appId={$settings.fb_appId}{/if}&amp;version=v2.0" async></script>
{/if}

{*** Twitter tweet ***}
{if isset($settings.social_twitter) and $settings.social_twitter}
<script id="twitter-wjs" src="//platform.twitter.com/widgets.js" async></script>
{/if}

{*** Google +1 ***}
{if isset($settings.social_google) and $settings.social_google}
<script src="https://apis.google.com/js/platform.js" async></script>
{/if}

{includeScript file='partials/bodyBottom.js.tpl'}
