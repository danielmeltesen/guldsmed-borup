{* Breadcrumb module *}
<div class="m-breadcrumb">
	<ul class="nav nav-breadcrumbs small">
		{foreach $page.breadcrumbs as $key => $breadcrumb}
			<li class="page level-{$key}{if $breadcrumb@last} active is-unclickable{/if}" {if !$breadcrumb@first}itemscope itemtype="http://data-vocabulary.org/Breadcrumb"{/if}>
				<a href="{$breadcrumb->path}" {if !$breadcrumb@first}itemprop="url"{/if}><span {if !$breadcrumb@first}itemprop="title"{/if}>{$breadcrumb->title}</span></a>
			</li>
			{if !$breadcrumb@last}
				<li class="seperator">/</li>
			{/if}
		{/foreach}
	</ul>
</div>
