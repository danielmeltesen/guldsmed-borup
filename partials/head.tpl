<meta charset="windows-1252">
<title>{$head.title}</title>
{*** Viewport setting for responsive websites ***}
<meta name="viewport" content="width=device-width, user-scalable=no">
{*** AJAX indexing ***}
<meta name="fragment" content="!">
{*** Generator tag ***}
<meta name="generator" content="{$general.generator}">
{*** Display all meta's added thru addMeta helper ***}
{foreach $head.metas as $meta}
{if !isset($meta->attributes.content) or $meta->attributes.content != null}<meta {foreach $meta->attributes as $k => $v}{$k}="{$v}" {/foreach}/>
{/if}
{/foreach}

{*** Favicon ***}
{if isset($template.settings.IMAGE_FAVICON) and !empty($template.settings.IMAGE_FAVICON)}
	<link rel="shortcut icon" href="{$template.settings.IMAGE_FAVICON}" type="image/x-icon" />
{/if}

<!--[if lte IE 9]>
<script src="{$template.cdn}{locateFile file="assets/js/ie.js"}"></script>
<link href="{$template.cdn}{locateFile file="assets/css/ie.css"}" rel="stylesheet" type="text/css">
<link href="{$template.cdn}{locateFile file="assets/css/template.ie.css"}" rel="stylesheet" type="text/css">
<![endif]-->

{*** Loads FontAwesome ***}
<link rel="stylesheet" href="{$template.cdn}/_design/common/libs/font-awesome/latest/css/font-awesome.min.css">

{*** Display all links added thru addStyle help ***}
{foreach $head.links as $link}<link {foreach $link->attributes as $k => $v}{$k}="{$v}" {/foreach}>
{/foreach}

{*** Add Google+ Author and Publisher ***}
{include file="modules/widgets/meta/google.tpl"}

{*** Google integrations ***}
{include file="modules/widgets/integrations/google_analytics.tpl"}
{include file="modules/widgets/integrations/google_tagsmanager.tpl"}

{*** Add JS to head from Design Manager ***}
{includeScript file='partials/head.js.tpl'}
