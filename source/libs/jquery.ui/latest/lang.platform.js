/* Danish initialisation for the jQuery UI date picker plugin. */
/* Written by Jan Christensen ( deletestuff@gmail.com). */
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "../datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}(function( datepicker ) {

	datepicker.regional['platform'] = {
		closeText: window.text.MODAL_CLOSE,
		prevText: window.text.PREVIOUS,
		nextText: window.text.NEXT,
		currentText: window.text.NOW,
		monthNames: [
			window.text.DATE_MONTH_JANUARY,
			window.text.DATE_MONTH_FEBRUARY,
			window.text.DATE_MONTH_MARCH,
			window.text.DATE_MONTH_APRIL,
			window.text.DATE_MONTH_MAY,
			window.text.DATE_MONTH_JUNE,
			window.text.DATE_MONTH_JULY,
			window.text.DATE_MONTH_AUGUST,
			window.text.DATE_MONTH_SEPTEMBER,
			window.text.DATE_MONTH_OCTOBER,
			window.text.DATE_MONTH_NOVEMBER,
			window.text.DATE_MONTH_DECEMBER
		],
		monthNamesShort: [
			window.text.DATE_MONTH_JANUARY.substring(0,3),
			window.text.DATE_MONTH_FEBRUARY.substring(0,3),
			window.text.DATE_MONTH_MARCH.substring(0,3),
			window.text.DATE_MONTH_APRIL.substring(0,3),
			window.text.DATE_MONTH_MAY.substring(0,3),
			window.text.DATE_MONTH_JUNE.substring(0,3),
			window.text.DATE_MONTH_JULY.substring(0,3),
			window.text.DATE_MONTH_AUGUST.substring(0,3),
			window.text.DATE_MONTH_SEPTEMBER.substring(0,3),
			window.text.DATE_MONTH_OCTOBER.substring(0,3),
			window.text.DATE_MONTH_NOVEMBER.substring(0,3),
			window.text.DATE_MONTH_DECEMBER.substring(0,3)
		],
		dayNames: [
			window.text.SUNDAY,
			window.text.MONDAY,
			window.text.TUESDAY,
			window.text.WEDNESDAY,
			window.text.THURSDAY,
			window.text.FRIDAY,
			window.text.SATURDAY
		],
		dayNamesShort: [
			window.text.DATE_DAY_SUNDAY.substring(0,3),
			window.text.DATE_DAY_MONDAY.substring(0,3),
			window.text.DATE_DAY_TUESDAY.substring(0,3),
			window.text.DATE_DAY_WEDNESDAY.substring(0,3),
			window.text.DATE_DAY_THURSDAY.substring(0,3),
			window.text.DATE_DAY_FRIDAY.substring(0,3),
			window.text.DATE_DAY_SATURDAY.substring(0,3)
		],
		dayNamesMin: [
			window.text.DATE_DAY_SUNDAY.substring(0,1),
			window.text.DATE_DAY_MONDAY.substring(0,1),
			window.text.DATE_DAY_TUESDAY.substring(0,1),
			window.text.DATE_DAY_WEDNESDAY.substring(0,1),
			window.text.DATE_DAY_THURSDAY.substring(0,1),
			window.text.DATE_DAY_FRIDAY.substring(0,1),
			window.text.DATE_DAY_SATURDAY.substring(0,1)
		],
		weekHeader: 'Week',
		//dateFormat: window.text.DATE_FORMAT.replace('%', '').replace('d', 'dd').replace('m', 'mm').replace('Y', 'yy'),
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};

	datepicker.setDefaults(datepicker.regional['platform']);

	return datepicker.regional['platform'];
}));
