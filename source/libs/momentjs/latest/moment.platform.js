;(function (moment) {
	'use strict';

	moment.lang('platform', {
		months: [
			window.text.DATE_MONTH_JANUARY,
			window.text.DATE_MONTH_FEBRUARY,
			window.text.DATE_MONTH_MARCH,
			window.text.DATE_MONTH_APRIL,
			window.text.DATE_MONTH_MAY,
			window.text.DATE_MONTH_JUNE,
			window.text.DATE_MONTH_JULY,
			window.text.DATE_MONTH_AUGUST,
			window.text.DATE_MONTH_SEPTEMBER,
			window.text.DATE_MONTH_OCTOBER,
			window.text.DATE_MONTH_NOVEMBER,
			window.text.DATE_MONTH_DECEMBER
		],
		weekdays: [
			window.text.DATE_DAY_SUNDAY,
			window.text.DATE_DAY_MONDAY,
			window.text.DATE_DAY_TUESDAY,
			window.text.DATE_DAY_WEDNESDAY,
			window.text.DATE_DAY_THURSDAY,
			window.text.DATE_DAY_FRIDAY,
			window.text.DATE_DAY_SATURDAY
		]
	});

	var replacements = {
		a: 'ddd',
		A: 'dddd',
		b: 'MMM',
		B: 'MMMM',
		d: 'DD',
		H: 'HH',
		I: 'hh',
		j: 'DDDD',
		m: 'MM',
		M: 'mm',
		p: 'A',
		S: 'ss',
		Z: 'z',
		w: 'd',
		y: 'YY',
		Y: 'YYYY',
		'%': '%'
	};

	moment.fn.strftime = function(format) {
		var momentFormat = format;

		for (var key in replacements) {
			momentFormat = momentFormat.replace("%" + key, replacements[key]);
		}

		return this.format(momentFormat);
	}
})(moment);