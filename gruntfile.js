module.exports = function(grunt) {
    require('time-grunt')(grunt);

    var js_source = [
            './source/libs/*/latest/**/*.js',
            './source/js/*.js',
            './modules/**/assets/js/*.js',
            './source/js/angularjs/lib/*.js',
            './source/js/angularjs/app/*.js',
            './source/js/angularjs/modules/**/*.js'
        ],
        js_source_ie = [
            './source/js/ie/*/latest/*.js'
        ];

    grunt.initConfig({

        //
        // watch for changes
        //
        watch: {
            options: {
                atBegin: true
            },
            sass: {
                files: ['./source/scss/**/*.{scss,sass}'],
                tasks: ['compass:watch','cssmin']
            },
            js: {
                files: ['./source/**/*.js','./modules/**/assets/js/*.js'],
                tasks: ['concat']
            }
        },


        //
        // sass compilation with compass
        //
        compass: {
            options: {
                require: 'breakpoint-slicer',
                sassDir: 'source/scss',
                cssDir: 'source/css',
                imagesDir: 'assets/images',
                specify: ['source/scss/template.structure.scss','source/scss/libs/ie/ie.scss','source/scss/template.ie.scss','source/scss/template.closed.scss','source/scss/print.scss'],
                relativeAssets: true,
                noLineComments: true,
                bundleExec: true
            },
            watch: {
                options: {
                    environment: 'development',
                    noLineComments: false
                }
            },
            build: {
                options: {
                    outputStyle: 'compressed',
                    environment: 'production',
                    noLineComments: true
                }
            }
        },

        cssmin: {
            combine: {
				options: {
					keepSpecialComments: 0
				},
                files: {
                    'assets/css/libs.css': ['./source/libs/*/latest/**/*.css', '!./source/libs/torqueui/latest/css/print.min.css', './source/js/angularjs/lib/*.css'],
                    'assets/css/ie.css' : ['./source/css/libs/ie/ie.css','./source/js/ie/*/latest/**/*.css'],
                    'assets/css/template.css': ['./source/css/template.structure.css'],
                    'assets/css/template.ie.css' : ['./source/css/template.ie.css'],
                    'assets/css/template.closed.css' : ['./source/css/template.closed.css'],
                    'assets/css/print.css': ['./source/libs/torqueui/latest/css/print.min.css', './source/css/print.css']
                }
            }
        },

        concat: {
            jswatch: {
                src: js_source,
                dest: 'assets/js/app.js'
            },
            jswatchIE: {
                src: js_source_ie,
                dest: 'assets/js/ie.js'
            }
        },

        uglify: {
            build: {
                files: {
                    'assets/js/app.js': js_source,
                    'assets/js/ie.js': js_source_ie
                },
                options: {
                    sourceMap: true,
                    preserveComments: false,
                    mangle: true,
                    compress: {
                        drop_console: true
                    }
                }
            }
        }

    });


    //
    // Use grunt-tasks to load modules instead of
    // grunt.loadNpmTasks('xxx');
    //
    require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});



    grunt.registerTask('default', ['watch']);
    grunt.registerTask('libs', ['cssmin', 'uglify:build']);
    grunt.registerTask('build', ['compass:build', 'libs']);
};
