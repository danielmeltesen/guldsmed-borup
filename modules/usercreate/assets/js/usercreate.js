;(function($) {
	'use strict';

	function init () {
		var $wrapper = $("#m-usercreate");

		if ($wrapper.length > 0) {
			// Execute form action on select change
			$('#m-usercreate-country', $wrapper).on('change', function(e){

				$('.state-group', $wrapper).addClass('is-hidden');
				$('.state-select', $wrapper).each(function() {
					$(this).val('');
				});

				var el = $('#m-usercreate-country').find(':selected');
				if (el.data('hasStates') > 0) {
					$('.state-group-' + el.val(), $wrapper).removeClass('is-hidden');
				}
			});

			// City/county locator
			$('#m-usercreate-zipcode', $wrapper).SmartForm('county', {target:'#m-usercreate-city'});

			// Country code locator
			$('#m-usercreate-country', $wrapper).SmartForm('countrycode', {
				target: function(code) {
					$('input.countryCode', $wrapper).val(code);
					$('span.countryCode', $wrapper).html('+' + code);
				}
			});

			// Interest group selector
			$('#m-usercreate-newsletter', $wrapper).on('change', function(e) {
				$('#m-usercreate-newsletterfields', $wrapper).toggleClass('is-hidden');
			});
		}
	}


	$(function() {
		init();
	});

})(jQuery, window);