{*

# Description
Controller for checkout page type, that handles all the checkout.


## Date last modified
2014-08-01


## Primary variables
+ $user                                                                 - Global scope variable containing the user
+ $page                                                                 - Global scope variable containing all information about the page type


## Partials (templates)
+ "/modules/widgets/modal/modal.tpl"                                    - Widget for modal window thru Fancybox
+ "/modules/checkout/partials/checkout-approved.tpl"                    - Checkout partial for when payment has been approved
+ "/modules/checkout/partials/checkout-declined.tpl"                    - Checkout partial for when payment has been declined
+ "/modules/checkout/partials/checkout-order.tpl"                       - Checkout partial for inputting customer data (also includes step 1 - 4)

*}

{if isset($smarty.get.approved) and !empty($page.orderId)}
	{include file='modules/checkout/checkout-approved.tpl'}
{elseif isset($smarty.get.declined)}
	{include file='modules/checkout/checkout-declined.tpl'}
{elseif isset($smarty.get.repay) and !empty($shop.checkout.repay.id)}
	{include file='modules/checkout/checkout-repay.tpl'}
{else}
	{collection controller=cart assign=cart}
	{if $cart->getActualSize() gt 0}
		{include file='modules/checkout/checkout-order.tpl'}
	{else}
		<div class="modules m-checkout">
			<header class="m-checkout-header page-title">
				<h1 class="m-contact-headline">{$text.CHECKOUT_HEADLINE}</h1>
			</header>

			<div class="panel panel-warning">
				<div class="panel-body text-center">
					{$text.CART_IS_EMPTY}
				</div>
			</div>
		</div>
	{/if}
{/if}
