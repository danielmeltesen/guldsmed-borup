{*

# Description
Checkout template partial, that is included when the payment has been approved. Part of Checkout page type.


## Date last modified
2014-08-01


## Primary variables
+ $text                                                                 - Global scope variable containing translations


## Partials (templates)
+ "/modules/widgets/order/order.tpl"                                    - Template widget for the User Order Details

*}

<div class="checkoutApproved">
    <div class="modules m-checkout-approved">
        <header class="m-checkout-approved-header page-title">
            <h1 class="m-checkout-approved-headline">{$text.CHECKOUT_STEP_FIVE_HEADLINE}</h1>
        </header>

    	<p class="m-checkout-approved text">{$text.CHECKOUT_STEP_FIVE_TEXT}</p>
        
        {if !empty($page.orderId)}
            {include file='modules/widgets/order/order.tpl' orderId=$page.orderId checkout=true}
        {/if}
    </div>
</div>