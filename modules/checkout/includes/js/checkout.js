;(function($, platform, exports) {
	'use strict';

	exports.platform = exports.platform || {};
	exports.platform.checkout =
	{
		firstload: true,
		loader: null,
		wrapper: null,
		ajaxUrl: '/actions/handler/ajax',
		load: function () {

			var self = this;

			self.wrapper = $('.checkoutFrame');

			self.loader = $('<img/>').attr({
				'class': 'loader',
				src: platform.template.cdn + '/_design/common/img/preloader/preloader-black.gif',
				alt: ''
			});

			// Bind events
			self.binds();

			if ($('.step3', self.wrapper).length) {
				self.STEP3.load();
			}

			self.STEP1.load();

			self.drawStepNumbers();
		},
		binds: function() {

			var self = this;

			if (!platform.getSetting('shop_delivery_hidden')) {
			    $(self.STEP1).on('addresschange', function() {
					$('#check_stage_all_loaded').val('0');
					self.STEP2.load();
				});
				$(self.STEP2).on('change', function() {
                    $('#check_stage_all_loaded').val('0');
                    self.STEP4.load();
				});
			}

			$(self.STEP1).on('countrychange', function() {
				$('#check_stage_all_loaded').val('0');
				if (!platform.getSetting('shop_delivery_hidden')) {
					self.STEP2.load();
				} else {
					self.STEP4.load();
				}
			});

			if (platform.getSetting('module_shop_packing')) {
				$('#check_stage_all_loaded').val('0');
                $(self.STEP3).on('change', function() {
					self.STEP4.load();
				});
			}
		},
		setVatRates: function() {
			var self = this;

			// When country changes, make sure to change the VAT calculations
			$('#check_stage_all_loaded').val('0');

            var fields = self.STEP1.relevantFieldDatas();

            $.get(platform.checkout.ajaxUrl, {
            	page: 'checkout',
            	action: 'delivery_country',
            	'delivery_country': fields.country,
            	company: fields.company,
            	cvr: fields.cvr,
            	createUserChecked: $('.step1 #createUser').is(':checked')
            }).then(function(data) {
                $(self.STEP1).trigger('countrychange');
			});
		},
		loaded: function(data) {
			var self = this;

			// Bind standard data to certain fields, containing their original value/ checked state
			$('.step1', self.wrapper).find('input:text').each(function() {
				$(this).data('pre', $(this).val());
			});

			$('.step2, .step3, .step4', self.wrapper).find('input:radio').each(function() {
				$(this).data('pre', $(this).is(':checked'));
			});

			// Bind a new event to textfields, it only triggers if the value changes from or to empty
			$('.step1', self.wrapper).find('input:text').on('keyup', function() {
				if (($(this).data('pre') == '' && $(this).val() != '') || ($(this).data('pre') != '' && $(this).val() == '')) {
					$(this).trigger('valuechange');
				}

				$(this).data('pre', $(this).val());
				self.STEP1.loadpostdk = true;
				self.STEP1.loadgls = true;
				self.STEP1.loadbringpp = true;
			});

			if (self.firstload) {

                $('.checkoutLoading').hide();

                self.wrapper.show();

                $(platform.checkout).trigger('firstdone', [data.result]);
			}

            $('#check_stage_all_loaded').val('1');

			self.firstload = false;

            $(self).trigger('done', [data.result]);
		},
		showMessage: function (type, message) {
			alert(message);
		},
		drawStepNumbers: function () {
			var i = 1;
			$('.stepNumber', self.wrapper).each(function () {
				$(this).text(i++ + '.');
			});
		},
		STEP1: {
			wrapper: null,
			// Activate step 1
			load: function() {

				this.wrapper = $('.step1');

				// Bind events
				this.binds();

				// Run actions
				this.actions();

				// Load triggers
				this.triggers();

				// Module loaded
				this.loaded();
			},
			// Bind actions to step1
			binds: function () {

				var self = this;

				// Toggle slide on activate delivery address checkbox
				$('#deliveryActive').on('change', function (e) {
					$('.deliveryContactInfo').slideToggle();
					if ($(this).is(':checked')) {
						$('.deliveryContactInfo input').attr('disabled', false);
					} else {
						$('.deliveryContactInfo input').attr('disabled', true);
					}
				});

				$('#newsletter').on('change', function (e) {
					self.toggleInterests();
				});

				// Bind slidetoggle to create user checkbox
				$('#createUser').on('change', function (e) {
					$('#inputPassword').slideToggle();
					self.toggleInterests();

					if ($(this).is(':checked')) {
						$('#inputPassword input').attr('disabled', false);
					} else {
						$('#inputPassword input').attr('disabled', true);
					}
				});

				if (platform.language.iso == 'DK') {
					$('#zipcode', self.wrapper).on('blur', function () {
						self.findCounty('normal');
					});

					$('#deliveryZipcode', self.wrapper).on('blur', function () {
						self.findCounty('delivery');
					});

					$('#city, #deliveryCity', self.wrapper).on('keyup', function () {
						$(this).removeData('autofilled');
					});
				}

				$('#country', self.wrapper).SmartForm('countrycode', {
					target: function (code) {
						$('.step1 span.countryCode').text('+' + code);
						$('.step1 input.countryCode').val(code);
						platform.checkout.setVatRates();
					}
				});

              	$('.country-select', self.wrapper).on('change', function(e) {
					if ($('#city').data('autofilled')) {
						$('#city').val('').removeData('autofilled');
					}

					// State
					$('.' + $(this).attr('id') + '-state-group', self.wrapper).addClass('is-hidden');
					$('.' + $(this).attr('id') + '-state-select', self.wrapper).each(function() {
						$(this).val('');
						$(this).attr('disabled', true);
					});

					var el = $(this).find(':selected');
					if (el.data('hasStates') > 0) {
						var $group = $('.' + $(this).attr('id') + '-state-group-' + el.val(), self.wrapper);
						$group.removeClass('is-hidden');
						$('select', $group).attr('disabled', false);
					}
              	});

              	$('#deliveryCountry', self.wrapper).on('change', function(e) {
					platform.checkout.setVatRates();
					if ($('#deliveryCity').data('autofilled')) {
						$('#deliveryCity').val('').removeData('autofilled');
					}
              	});
			},
			// Trigger needed actions
			actions: function () {

				// Check the activate delivery address checkbox if active
				if ($('#deliveryActive').is(':checked')) {
					$('#deliveryActive').trigger('change');
					platform.checkout.setVatRates();
				}

				// Check the create user checkbox if active
				if ($('#createUser').is(':checked')) {
					$('#createUser').trigger('change');
				}

				// Check the newsletter checkbox if active
				if ($('#newsletter').is(':checked')) {
					$('#newsletter').trigger('change');
				}
			},
			// Certain field changes needs to trigger a change in the oter steps
			triggers: function() {
				var self = this;

              	$('#zipcode, #address, #deliveryZipcode, #deliveryAddress', self.wrapper).on('change', function(e) {
                    $(self).trigger('addresschange');
              	});

              	$('#vatNumber, #company, #deliveryCompany', self.wrapper).on('valuechange', function(e) {
					platform.checkout.setVatRates();
              	});

              	$('#deliveryActive, #createUser', self.wrapper).on('change', function(e) {
					platform.checkout.setVatRates();
              	});
			},
			findCounty: function(type) {
				if (type == 'normal') {
					var zipField = '#zipcode';
					var cityField = '#city';
					var countryField = '#country';
				} else {
					var zipField = '#deliveryZipcode';
					var cityField = '#deliveryCity';
					var countryField = '#deliveryCountry';
				}

				if ($(zipField).val().length == 4 && $(countryField).val() == 'DK') {
					$.getJSON(platform.checkout.ajaxUrl, {
						page: 'helper',
						action: 'find_county',
						zip: $(zipField).val(),
						country: $(countryField).val().toLowerCase()
					}).then(function(data) {
	                    if (data.result) {
	                        $(cityField).val(data.result);
	                        $(cityField).data('autofilled', '1').blur();
	                    }
	                });
	            }
			},
			// Toggles interest fields
			toggleInterests : function() {
                var newsbox = $("#newsletter").is(':checked');
                var userbox = $("#createUser").is(':checked');

                var interests = $("#interests_customer");
                var interests_customer = $("#interests");

                $('.interestsContainer input').checked(false);
                var el = (newsbox ? (userbox ? interests : interests_customer) : '');
                if (el != '') {
                    if (interests != el) interests.slideUp();
                    else interests_customer.slideUp();
                    el.slideDown();
                } else {
                    interests.slideUp();
                    interests_customer.slideUp();
                }
            },
            relevantFieldDatas: function(noDelivery) {
				if (noDelivery != true && $('.step1 #deliveryActive').is(':checked')) {
					var eventData = {
						country: $('.step1 #deliveryCountry').val(),
						zip: $('.step1 #deliveryZipcode').val(),
						city: $('.step1 #deliveryCity').val(),
						address: $('.step1 #deliveryAddress').val(),
						company: $('.step1 #company').length ? $('.step1 #company').val() : '',
						cvr: $('.step1 #vatNumber').length ? $('.step1 #vatNumber').val() : '',
						ean: $('.step1 #eanNumber').length ? $('.step1 #eanNumber').val() : ''
					};
                } else {
					var eventData = {
						country: $('.step1 #country').val(),
						zip: $('.step1 #zipcode').val(),
						city: $('.step1 #city').val(),
						address: $('.step1 #address').val(),
						company: $('.step1 #company').length ? $('.step1 #company').val() : '',
						cvr: $('.step1 #vatNumber').length ? $('.step1 #vatNumber').val() : '',
						ean: $('.step1 #eanNumber').length ? $('.step1 #eanNumber').val() : ''
					};
                }

                eventData.deliveryActive = $('.step1 #deliveryActive').is(':checked');
                eventData.deliveryCompany = eventData.deliveryActive ? $('.step1 #deliveryCompany').val() : '';

                return eventData;
	        },
	        showDeliveryAddressOption: function(show) {
	        	var deliveryAddressOption = $('.step1 .deliveryContactInfoContainer');

	        	if (show) {
	        		deliveryAddressOption.show();
	        	} else {
	        		deliveryAddressOption.hide();
	        	}
	        },
	        loaded: function(data) {
				$(this).trigger('countrychange');
			},
			loadpostdk: true,
			loadgls: true,
			loadbringpp: true
		},
		STEP2 : {
			wrapper: null,
			firstload: true,
            load: function() {
            	var self = this;

            	self.wrapper = $('.step2');

                if (self.firstload) {
                	self.binds();
                	self.firstload = false;
                }

            	self.deliveryMethods();
            },
			deliveryMethods: function() {
				var self = this,
                	step1fields = platform.checkout.STEP1.relevantFieldDatas(),
                	step2fields = this.relevantFieldDatas();

                $.getJSON(platform.checkout.ajaxUrl, {
		        	page: 'checkout',
		        	action: 'delivery_methods',
		        	'delivery_country': step1fields.country,
		        	'delivery_zip': step1fields.zip,
		        	cvr: (step1fields.deliveryActive ? (step1fields.deliveryCompany != '' ? '1' : '') : (step1fields.company != '' ? '1' : '')),
		        	'delivery_address': step1fields.deliveryActive
		    	}).then(function(data) {
                    $('.deliveryMethodContainerMain', self.wrapper).empty();
                    if (data.result.length) {
						$('.deliveryMethodNotFound', self.wrapper).hide();
						$('.deliveryMethodContainerMain', self.wrapper).html($('#deliveryMethodTemplate').render( data.result ));
                    } else {
						$('.deliveryMethodNotFound', self.wrapper).show();
                        $(self).trigger('change');
                    }

                    // Restore state if any
                    if (step2fields.deliveryMethodIds != '') {
                        $(step2fields.deliveryMethodIds.split(',')).each(function() {
                            $('.deliveryMethodSelector input[value="'+this+'"]', self.wrapper).checked(true);
                        });
                    } else {
                    	var deliveryMethod = $('input[name="post_delivery_method"]', self.wrapper).val();
                        if (deliveryMethod != '') {
                            $('input[name="delivery_method"][value="' + deliveryMethod + '"]', self.wrapper).checked(true);
                        }

                        $('input.post_delivery_method_fixed', self.wrapper).each(function() {
                        	var k = $(this).attr('name').replace('post_', '');
                        	var v = $(this).val();
                        	$('input[name="delivery_method_'+k+'"][value="' + v + '"]', self.wrapper).checked(true);
                        });
                    }

                    // Check for pre-checked methods and trigger them
                    var methods = $('input.deliveryMethodRadio:checked', self.wrapper);
                    if (methods.length) {
                    	methods.trigger('change');

                    // If several groups exists, check all the firsts, but trigger only one
					} else if ($('.deliveryMethodGroupContainer').length > 1) {
						$('.deliveryMethodGroupContainer').find('input.deliveryMethodRadio:first').checked(true).first().trigger('change');

					// If none or not enough groups exists, check and trigger the very first delivery method
					} else {
						$('input.deliveryMethodRadio:first', self.wrapper).checked(true).trigger('change');
					}

                    self.loaded(data);
			    });
		    },
		    glsPromise: null,
		    glsLocations: function(address, zip) {

				if (platform.checkout.STEP1.loadgls) {
		        	this.glsPromise = $.getJSON(platform.checkout.ajaxUrl, {
		        		page: 'checkout',
		        		action: 'gls_locations',
						'delivery_address': address,
		        		'delivery_zip': zip
		        	});

		        	platform.checkout.STEP1.loadgls = false;

			    }

			   	return this.glsPromise;
		    },
		    postdkPromise: null,
		    postdkLocations: function(address, zip, country) {

				if (platform.checkout.STEP1.loadpostdk) {
		        	this.postdkPromise = $.getJSON(platform.checkout.ajaxUrl, {
		        		page: 'checkout',
		        		action: 'postdk_locations',
						'delivery_country': country,
		        		'delivery_address': address,
		        		'delivery_zip': zip
		        	});

		        	platform.checkout.STEP1.loadpostdk = false;
				}

			    return this.postdkPromise;
		    },
		    bringppPromise: null,
		    bringppLocations: function(address, zip, country) {

				if (platform.checkout.STEP1.loadbringpp) {
		        	this.bringppPromise = $.getJSON(platform.checkout.ajaxUrl, {
		        		page: 'checkout',
		        		action: 'bringpp_locations',
						'delivery_country': country,
		        		'delivery_address': address,
		        		'delivery_zip': zip
		        	});

		        	platform.checkout.STEP1.loadbringpp = false;
				}

			    return this.bringppPromise;
		    },
		    binds: function() {

		    	var self = this;

		    	$(self.wrapper).on(
			    	{
			    		change: function (e) {
                			var fields = platform.checkout.STEP1.relevantFieldDatas(true),
			    				$this = $(this).parents('.deliveryMethodContainer'),
			    				$container = $('.locationContainer', $this),
			    				promise = null,
			    				template = null;

			    			$('.locationContainer', self.wrapper).hide();

							var hadDroppoints = false;

			    			if (fields.zip.length >= 4) {
				    			if ($(this).hasClass('gls')) {
									promise = self.glsLocations(fields.address, fields.zip);
									template = $('#glsLocationTemplate');
									hadDroppoints = true;
								}
								else if ($(this).hasClass('postdk')) {
									promise = self.postdkLocations(fields.address, fields.zip, fields.country);
									template = $('#postdkLocationTemplate');
									hadDroppoints = true;
								}
								else if ($(this).hasClass('bringpp')) {
									promise = self.bringppLocations(fields.address, fields.zip, fields.country);
									template = $('#bringppLocationTemplate');
									hadDroppoints = true;
								}
							}

							if (hadDroppoints) {
								platform.checkout.STEP1.showDeliveryAddressOption(false);
							} else {
								platform.checkout.STEP1.showDeliveryAddressOption(true);
							}

							if (promise && template) {

								var $target = $('.target', $this);

				                $target.empty();

				                $target.append(platform.checkout.loader);

				                $container.show();

								promise.then(function (data) {

									if (data.result.length) {
			                            $target.html(template.render(data.result));
				                        $('input:radio', $container).first().checked(true);
				                    } else {
                                        template = $('#emptyLocationTemplate');
                                        $target.html(template.render());
                                    }
                                    self.droppointsLoaded(data.result, $target);
							    });
							}

							$(self).trigger('change');
			    		}
			    	},
			    	'input.deliveryMethodRadio'
			    );
		    },
			relevantFieldDatas: function() {
				var vals = [];
				$('.deliveryMethodSelector input:radio:checked', self.wrapper).each(function() {
					vals.push($(this).val())
				});

				return {
					deliveryMethodIds: vals.join(',')
				}
	        },
	        droppointsLoaded: function(locations, target) {
	        	$(platform.checkout).trigger('droppointsLoaded', [
		        	{
		        		locations: locations,
		        		target: target
		        	}
		        ]);
	        },
	        loaded: function(data) {
				$(platform.checkout).trigger('deliveryMethodsLoaded', [data.result]);
			}
		},
		STEP3: {
			wrapper: null,
			load: function() {
				var self = this;

				self.wrapper = $('.step3');

				self.binds();

				$.getJSON(platform.checkout.ajaxUrl, {
					page: 'checkout',
					action: 'giftwrappings'
				}).then(function (data) {

					// If giftwrappings were returned, render the template
	        		if (data.code == 0 && data.result && data.result.length) {
	        			$('.giftWrappingLeftContainer', self.wrapper).html( $('#giftWrappingTemplate').render( data.result ) );
						self.triggers();
	        		}

	        		// Else hide the giftwrappings box
	        		else {
	        			self.wrapper.remove();
	        			platform.checkout.drawStepNumbers();
	        		}
	        	});

            },
            relevantFieldDatas: function() {
				return {
				 	wrappingId: $('input[name="packingType"]:checked', self.wrapper).val()
				}
			},
			triggers: function () {

				// Restore state
				var state = $('input[name="post_packingType"]', self.wrapper).val();
				if (state != '') {
					$('input:radio[name="packingType"][value="' + state + '"]', self.wrapper).checked(true).trigger('change');
				}

				// Auto click the first choice
				else if (!$('input:radio:checked', self.wrapper).length) {
					$('input:radio', self.wrapper).first().checked(true);
				}
			},
			binds: function () {
				var self = this;
				$(self.wrapper).on('change', 'input:radio', function() {
                    $(self).trigger('change');
              	});
			}
		},
		STEP4: {
			wrapper: null,
			firstload: false,
			load: function() {
				var self = this;

				self.wrapper = $('.step4');

				if (!self.firstload) {
					self.binds();
					self.firstload = true;
				}

				self.paymentMethods();
			},
			relevantFieldDatas: function() {
				var self = this;
				return {
					paymentMethod: $('input.paymentMethodRadio:checked', self.wrapper).val(),
					paymentMethodOnline: $('input.paymentMethodOnlineRadio:checked', self.wrapper).val()
				};
	        },
			paymentMethods: function () {
				var self = this;

				var step1fields = platform.checkout.STEP1.relevantFieldDatas();
				var step2fields = platform.checkout.STEP2.relevantFieldDatas();
				var step3fields = platform.checkout.STEP3.relevantFieldDatas();
				var step4fields = this.relevantFieldDatas();

				$.getJSON(platform.checkout.ajaxUrl, {
					page: 'checkout',
					action: 'payment_methods',
					'delivery_country': step1fields.country,
                    'delivery_zip': step1fields.zip,
					'delivery_methods': step2fields.deliveryMethodIds,
					company: step1fields.company,
					country: step1fields.country,
					cvr: step1fields.cvr,
					giftWrapping: step3fields.wrappingId
				}).then(function(data) {
					var $container = $('.paymentMethodContainerMain', self.wrapper),
						$notfound = $('.paymentMethodNotFound', self.wrapper);

					$container.empty();

               		if (data.result.length) {
						$notfound.hide();

						$container.append(
							$('#paymentMethodTemplate').render(
								data.result,
								{
									paymentGraphic: function(logo) {
										return logo ? '<img alt="" src="' + platform.template.cdn + logo.split('|').join('"><img alt="" src="' + platform.template.cdn) + '">' : '';
									}
								}
							)
						);
                    } else {
						$notfound.show();
						self.orderSummary();
                    }

                    // Find the top payment method in the list, this is the default method
                    var paymentMethod = $('input[name="paymentType"]:first', self.wrapper).val();

					// Restore previous selection if there was one and it still exists
					if (step4fields.paymentMethod && $('input[name="paymentType"][value="' + step4fields.paymentMethod + '"]', self.wrapper).length) {
						paymentMethod = step4fields.paymentMethod;
					}
					// Restore from session if the information is there
					else {
						var postPaymentType = $('input[name="post_paymentType"]', self.wrapper).val();
						if (postPaymentType != '' && $('input[name="paymentType"][value="' + postPaymentType + '"]', self.wrapper).length) {
							paymentMethod = postPaymentType;
						}
					}

					var $paymentMethod = $('input[name="paymentType"][value="' + paymentMethod + '"]:first', self.wrapper);

					// Check selected payment method
					$paymentMethod.checked(true);

					// Enable all sub-inputs for selected payment method
					self.selectPaymentMethod($paymentMethod);

					if (step4fields.paymentMethodOnline && $('#pbs_type_' + step4fields.paymentMethodOnline, self.wrapper).length) {
						$('#pbs_type_' + step4fields.paymentMethodOnline, self.wrapper).checked(true);
					}
					else {
						var postOnlinePaymentType = $('input[name="post_onlinePaymentType"]', self.wrapper).val();
						if (postOnlinePaymentType != '') {
							$('#pbs_type_' + postOnlinePaymentType, self.wrapper).checked(true);
						} else {
							$('input.paymentMethodOnlineRadio:first', self.wrapper).checked(true);
						}
					}

                    self.triggers();

                    $(platform.checkout).trigger('paymentMethodsLoaded', [data.result]);
				});
			},
			selectPaymentMethod: function (selected) {
				var $this = $(selected);
				$this.trigger('showactive');
				if ($this.val() == 1) {
					$('.paymentMethodOnlineContainerMain input').attr('disabled', false);
				} else {
					$('.paymentMethodOnlineContainerMain input').attr('disabled', true);
				}
			},
			orderSummary: function () {

				var self = this,
					wrapper = $('.step5'),
					step1fields = platform.checkout.STEP1.relevantFieldDatas(),
					step2fields = platform.checkout.STEP2.relevantFieldDatas(),
					step3fields = platform.checkout.STEP3.relevantFieldDatas(),
					step4fields = self.relevantFieldDatas();

				$.getJSON(platform.checkout.ajaxUrl, {
					page: 'checkout',
					action: 'order_summary',
					'delivery_country': step1fields.country,
					cvr: (step1fields.deliveryActive ? (step1fields.deliveryCompany != '' ? '1' : '') : (step1fields.company != '' ? '1' : '')),
					company: step1fields.company,
					'delivery_methods': step2fields.deliveryMethodIds,
					giftWrapping: step3fields.wrappingId,
					'payment_method': step4fields.paymentMethod,
					'payment_method_online': step4fields.paymentMethodOnline
				}).then(function(data) {
					var orderData = $.extend( data.result, {
						orderLineLimit: 9
					});
                    $('.orderSummary', wrapper).html($('#orderSummaryTemplate').render( orderData ));
                    $('#check_stage_all_loaded').val('1');
                    $('.orderline-hidden-button', wrapper).on('click', function (e) {
                    	$('.orderLine.is-hidden', wrapper).removeClass('is-hidden');
                    	$('.orderLine.l-orderline-hidden-info', wrapper).fadeOut().remove();
                    });
					self.loaded(data);
			    });
			},
			binds: function() {
			 	var self = this;

				$(self.wrapper).on('change', 'input[name="onlinePaymentType"]', function() {
					self.orderSummary();
				});

				$(self.wrapper).on('change', 'input[name="paymentType"]', function() {
					self.selectPaymentMethod(this);
					self.orderSummary();
				});
		    },
		    triggers: function () {
		    	var self = this;
		    	self.orderSummary();
		    },
			loaded: function(data) {

                // Hide loading icon
				platform.checkout.loaded(data);
			}
		}
	};

	$(function() {
		if (platform.page.type == 'checkout' && $('.checkoutFrame').length) {
			exports.platform.checkout.load();
		}
	});

})(jQuery, window.platform, window);
