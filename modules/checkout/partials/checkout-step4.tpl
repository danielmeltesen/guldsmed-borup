{*

# Description
Template partial for Checkout step 4 of the checkout process. Part of Checkout page type.
Consisting mostly of a [jsRender Template](http://www.jsviews.com/#jsrender).


## Date last modified
2014-01-21


## Resource
+ [jsRender Template](http://www.jsviews.com/#jsrender)


## Primary variables


## Partials (templates)
No extra templates required for this template

*}

<input type="hidden" name="post_paymentType" value="{$returnPostData.paymentType}">
<input type="hidden" name="post_onlinePaymentType" value="{$returnPostData.onlinePaymentType}">

{literal}
<script id="paymentMethodTemplate" type="text/x-jsrender">
<div class="paymentMethodContainer" id="paymentMethodContainer_{{:id}}">
	<div class="paymentMethodSelector radio">
		<fieldset class="form-group">

			<div class="input-group">

				<span class="input-group-addon">
					<input id="payment_type_{{:id}}{{if id == 9}}_klarna_{{:klarna.group}}{{/if}}" type="radio" name="paymentType" value="{{:id}}" class="paymentMethodRadio" data-show-on-active=".payment_details_{{:id}}{{if id == 9}}_{{:klarna.group}}{{/if}}" data-show-active-group="payment">
				</span>

				<div class="form-label input-group-main">

					<label for="payment_type_{{:id}}{{if id == 9}}_klarna_{{:klarna.group}}{{/if}}">

						<span class="paymentMethodTitle">
							{{if id == 9}}
								<span class="l-payment-method-title-logo klarnaLogo is-block">
									<img alt="Klarna" src="https://cdn.klarna.com/1.0/shared/image/generic/logo/{{:klarna.locale.toLowerCase()}}/basic/blue-black.png?width=100&eid={{:klarna.paymentCode}}">
								</span>
							{{/if}}
							<span class="l-payment-method-title-text">
								{{:title}}{{if subtitle}}: <small>{{:subtitle}}</small>{{/if}}
							</span>
						</span>

						{{if priceFixed}}
							<span class="paymentMethodFeeFixed is-block">{{:priceFixed}}</span>
						{{/if}}
						{{if pricePercentage}}
							<span class="paymentMethodFeePercentage is-block">{{:pricePercentage}} %</span>
						{{/if}}

						<span class="paymentMethodDescription is-block">{{:description}}</span>

						{{if id == 6}}
							<span class="paymentMethodPaypalIcons is-block">
								<img src="/_design/common/img/payment/card_mastercard.gif" alt="Mastercard">
								<img src="/_design/common/img/payment/card_visa.gif" alt="Visa">
								<img src="/_design/common/img/payment/card_americanexpress.gif" alt="American Express">
								<img src="/_design/common/img/payment/card_discover.gif" alt="Discover">
								<img src="/_design/common/img/payment/card_echeck.gif" alt="E-Check">
								<img src="/_design/common/img/payment/card_paypal.gif" alt="PayPal">
							</span>

						{{else id == 8}}
							<span class="paymentMethodPaypalIcons is-block">
								<img src="/_design/common/img/payment/card_paybybill.png" alt="PayByBill">
							</span>
						{{/if}}
					</label>

					{{if id == 8}}
						{{if paybybill.showValidation}}
							<div id="paybybillcontainer" class="payment_details_{{:id}}" style="display:none;">
								<fieldset class="form-group m-paybybill-label">
									<label class="form-label" for="cprPayBybill">
										{{:paybybill.info.fieldLabel}} <span class="form-required">*</span>
									</label>
									<input class="form-input" type="text" id="cprPayByBill" name="cprPayBybill" placeholder="{{:paybybill.info.fieldText}}" disabled required autocomplete="off">
								</fieldset>
							</div>
						{{/if}}

					{{else id == 9}}
						<div class="m-klarna-container payment_details_{{:id}}_{{:klarna.group}}" style="display:none;">

							{{if klarna.paymentMethods}}
								{{if klarna.paymentMethods.length == 1}}
									<div class="m-klarna-payment-methods">
										<input type="hidden" name="klarna_pclass" value="{{:klarna.paymentMethods[0].pclass}}" disabled>
										{{if klarna.paymentMethods[0].details}}
										<table class="table-h-striped">
											<tbody>
												{{for klarna.paymentMethods[0].details}}
												<tr>
													<td>{{:label}}</td>
													<td>{{:value}} {{:symbol}}</td>
												</tr>
												{{/for}}
											</tbody>
										</table>
										{{/if}}

										{{if klarna.paymentMethods[0].exampleText || klarna.paymentMethods[0].terms}}
										<p class="klarna_example">
											<span class="m-klarna-exampletext is-block">{{:klarna.paymentMethods[0].exampleText}}</span>
											<a class="m-klarna-terms-button is-block fancybox fancybox.iframe" href="{{:klarna.paymentMethods[0].terms}}">{{:~text('PRODUCT_CATALOG_READ_MORE')}}</a>
										</p>
										{{/if}}
									</div>
								{{else}}
								<ul class="m-klarna-payment-methods list-unstyled">
									{{for klarna.paymentMethods}}
									<li>
										<fieldset class="form-group">
										    <div class="input-group" style="width:100%;">
										        <span class="input-group-addon">
										        	<input id="klarna_type_{{:group}}_{{:#index}}" class="m-klarna-payment-methods-choice" type="radio" name="klarna_pclass" value="{{:pclass}}" required disabled data-show-on-active="#klarna_details_{{:group}}_{{:#index}}" data-show-active-group="klarna">
										        </span>
										        <div class="form-label input-group-main">
										        	<label for="klarna_type_{{:group}}_{{:#index}}" class="paymentMethodTitle">{{:title}}</label>

													<div id="klarna_details_{{:group}}_{{:#index}}" class="klarnaShowOnClick" style="display:none;">
														{{if details}}
														<table class="table-h-striped">
															<tbody>
																{{for details}}
																<tr>
																	<td>{{:label}}</td>
																	<td>{{:value}} {{:symbol}}</td>
																</tr>
																{{/for}}
															</tbody>
														</table>
														{{/if}}

														{{if exampleText || terms}}
														<p class="klarna_example">
															<span class="m-klarna-exampletext is-block">{{:exampleText}}</span>
															<a class="m-klarna-terms-button is-block fancybox fancybox.iframe" href="{{:terms}}">{{:~text('PRODUCT_CATALOG_READ_MORE')}}</a>
														</p>
														{{/if}}
													</div>
										        </div>
										    </div>
										</fieldset>
									</li>
									{{/for}}
								</ul>
								{{/if}}
							{{/if}}

							<fieldset class="form-group m-klarna-label">
								<label class="form-label" for="cpr_{{:group}}">
									{{:klarna.fieldLabel}} <span class="form-required">*</span>
								</label>
								<input class="form-input" type="text" id="cpr_{{:group}}" name="cpr" placeholder="{{:klarna.fieldText}}" disabled required autocomplete="off">
							</fieldset>

							{{if klarna.extraText}}
								<div class="m-klarna-extra-text">
									{{:klarna.extraText}}
								</div>
							{{/if}}
						</div>
					{{/if}}

				</div>

			</div>
		</fieldset>
	</div>

	{{if onlineMethods}}
	<div class="paymentMethodOnlineContainerMain showOnClick">
		<ul class="list-unstyled" style="padding: 0 0 0 1.5em;">
		{{for onlineMethods}}
		<li>
			<fieldset class="form-group small">
				<div class="input-group">
					<span class="input-group-addon">
						<input type="radio" name="onlinePaymentType" id="pbs_type_{{:id}}" value="{{:id}}" class="paymentMethodOnlineRadio" disabled>
					</span>
					<label for="pbs_type_{{:id}}" class="form-label input-group-main">
						<span class="paymentMethodOnlineName is-block">{{:title}}</span>
						{{if priceFixed}}
							<span class="paymentMethodOnlineFeeFixed is-block">{{:priceFixed}}</span>
						{{/if}}
						{{if pricePercentage}}
							<span class="paymentMethodOnlineFeePercentage is-block">{{:pricePercentage}} %</span>
						{{/if}}
						<span class="paymentMethodOnlineIcons is-block">
							{{:~paymentGraphic(logo)}}
						</span>
					</label>
				</div>
			</fieldset>
		</li>
		{{/for}}
		</ul>
	</div>
	{{/if}}
</div>
</script>
{/literal}

<div class="paymentMethodContainerMain"></div>