{*

# Description
Template partial for Checkout step 1 of the checkout process. Part of Checkout page type.


## Date last modified
2014-08-01


## Primary variables
+ $User                                                    				- Global scope variable containing user data
+ $deliveryCountries													- Collection of the deliveryCountryController (countries we deliver to)


## Partials (templates)
+ "/modules/widgets/order/order.tpl"                        			- Template widget for the User Order Details
+ "/modules/checkout/partials/checkout-step1-user.tpl" 					- Template partial to fetch user data from $User
+ "/modules/checkout/partials/checkout-step1-customer.tpl"  			- Template partial to ask user for data

*}

{* Fetch delivery countrys *}
{collection controller=deliveryCountry assign=deliveryCountries access=true}

{* If the user is logged in, display the users information in step 1 *}
{if $user}
	{include file='modules/checkout/partials/checkout-step1-user.tpl'}
{* No user logged in *}
{else}
	{include file='modules/checkout/partials/checkout-step1-customer.tpl'}
{/if}

{if $settings.checkout_show_delivery_address}

<div class="deliveryContactInfoContainer">
	<div class="deliveryContactInfoDisplay checkbox">
		<fieldset id="m-checkout-del" class="form-group">
			<div class="input-group">
				<span class="input-group-addon"><input type="checkbox" class="radiobtn" name="deliveryActive" id="deliveryActive" {if $returnPostData.deliveryActive}checked{/if} /></span>
				<label for="deliveryActive" class="form-label input-group-main">{$Text.CHECKOUT_DELIVERY_ADDRESS_TEXT}</label>
			</div>
		</fieldset>
	</div>

	<div class="deliveryContactInfo" style="display: none;">
		<fieldset id="m-checkout-del-firstname" class="form-group">
			<label class="form-label" for="m-checkout-del-firstname">{$Text.FIRSTNAME} <span class="form-required">*</span></label>
			<input type="text" name="deliveryFirstname" value="{$returnPostData.deliveryFirstname}" id="deliveryFirstname" class="form-input" placeholder="{$Text.FIRSTNAME}" disabled required>
		</fieldset>

		<fieldset class="form-group m-checkout-del-lastname">
			<label class="form-label" for="m-checkout-del-lastname">{$Text.LASTNAME} <span class="form-required">*</span></label>
			<input type="text" name="deliveryLastname" value="{$returnPostData.deliveryLastname}" id="deliveryLastname" class="form-input" placeholder="{$Text.LASTNAME}" disabled required>
		</fieldset>

		{if $settings.shop_customer_company}
		<fieldset class="form-group m-checkout-del-company">
			<label class="form-label" for="m-checkout-del-company">{$Text.COMPANY} {$Text.USER_ONLY_FOR_COMPANIES}</label>
			<input type="text" name="deliveryCompany" value="{$returnPostData.deliveryCompany}" id="deliveryCompany" class="form-input" placeholder="{$Text.COMPANY}" disabled>
		</fieldset>
		{/if}

		<fieldset class="form-group m-checkout-del-address">
			<label class="form-label" for="m-checkout-del-address">{$Text.ADDRESS} <span class="form-required">*</span></label>
			<input type="text" name="deliveryAddress" value="{$returnPostData.deliveryAddress}" id="deliveryAddress" class="form-input" placeholder="{$Text.ADDRESS}" disabled required>
		</fieldset>

		<fieldset class="form-group m-checkout-del-zipcode">
			<label class="form-label" for="m-checkout-del-zipcode">{$Text.POSTCODE} <span class="form-required">*</span></label>
			<input type="text" name="deliveryZipcode" value="{$returnPostData.deliveryZipcode}" id="deliveryZipcode" placeholder="{$Text.POSTCODE}" autocomplete="off" class="form-input" disabled required>
		</fieldset>

		<fieldset class="form-group m-checkout-del-city">
			<label class="form-label" for="m-checkout-del-city">{$Text.CITY} <span class="form-required">*</span></label>
			<input type="text" name="deliveryCity" value="{$returnPostData.deliveryCity}" id="deliveryCity" class="form-input" placeholder="{$Text.CITY}" disabled required>
		</fieldset>

		<div class="deliveryCountry">
		{if $deliveryCountries->getActualSize() gt 1}
			<fieldset class="form-group m-checkout-del-country">
        		<label class="form-label" for="m-checkout-del-country">{$Text.COUNTRY} <span class="form-required">*</span></label>
				<select id="deliveryCountry" class="country-select form-input form-select small" name="deliveryCountry">
					{foreach $deliveryCountries->getData() as $country}
						<option data-has-states="{count($country->CountryStates)}" value="{$country->Iso}"
						{if $country->Iso eq $returnPostData.deliveryCountry or $country->Iso eq $general.deliveryCountryIso or $country->Primary}selected{/if}>{$country->Title}</option>
					{/foreach}
				</select>
			</fieldset>
		{else}
			<input type="hidden" id="deliveryCountry" name="deliveryCountry" value="{$general.deliveryCountryIso}">
		{/if}

		{foreach $deliveryCountries->getData() as $country}
			{if count($country->CountryStates) gt 0}
				<fieldset class="form-group deliveryCountry-state-group deliveryCountry-state-group-{$country->Iso} {if $general.deliveryCountryIso != $country->Iso}is-hidden{/if}">
	        		<label for="state" class="form-label">{$Text.STATE} <span class="form-required">*</span></label>
					<select id="deliveryState{$country->Id}" class="deliveryCountry-state-select form-input form-select small" name="deliveryState">
						{foreach $country->CountryStates as $DeliveryStateAbbr => $DeliveryState}
							<option value="{$DeliveryStateAbbr}" {if $DeliveryStateAbbr eq $returnPostData.deliveryState}selected{/if}>{$DeliveryState}</option>
						{/foreach}
					</select>
				</fieldset>
			{/if}
		{/foreach}
		</div>
	</div>
</div>
{/if}

{if $settings.checkout_reference_number}
<div class="referenceNumber">
	<fieldset class="form-group m-checkout-del-reference">
		<label class="form-label" for="m-checkout-del-reference">{$Text.MY_ORDERS_REFERENCE_NUMBER}{if $settings.checkout_reference_number_required} <span class="form-required">*</span>{/if}</label>
		<input type="text" name="referenceNumber" value="{$returnPostData.referenceNumber}" id="referenceNumber" class="form-input" placeholder="{$Text.MY_ORDERS_REFERENCE_NUMBER}" {if $settings.checkout_reference_number_required}required{/if}>
	</fieldset>
</div>
{/if}
