{*

# Description
Controller and template for Text page type.


## Date last modified
2014-08-01


## Primary variables
+ $text                                                                 - Global scope variable containing translations


## Partials (templates)
+ "/modules/widgets/meta/opengraph.tpl"                                 - Social meta data

*}

{if $page and $page.id}
    {*** Global widgets defaults ***}
    {$prev = NULL}
    {$next = NULL}

    {$opengraph_image = NULL}
    {$opengraph_title = $page.headline}
    {$opengraph_description = NULL}

    {controller type=page assign=controller primary=true}
    {$pageText = $controller->getText($page.id)}
    {$opengraph_description = $pageText|strip_tags|truncate:160}

    {entity assign=pageImage controller=files type=page id=$page.id pageSize=1}
    {if $pageImage}
        {$opengraph_image = $pageImage->thumbnail(480)|solutionPath}
    {/if}

    <div class="modules m-text" itemscope itemtype="http://schema.org/Article">
        <article class="m-text-article">
            {$pageHeadline = trim($page.headline)}
            {if !empty($pageHeadline)}
            <header class="m-text-header page-title {if $page.isFrontPage}page-title-frontpage{/if}">
                <h1 class="m-text-headline" itemprop="headline">{$page.headline}</h1>
            </header>
            {/if}

            {*** Page content ***}
            <div class="m-text-content content clearfix" itemprop="description">{$pageText}</div>
        </article>
    </div>

    {*** Global widgets ***}
    {include file="modules/widgets/meta/opengraph.tpl"
        og=["title" => $opengraph_title, "description" => $opengraph_description, "image" => $opengraph_image]
    }
{else}
    {notfound}
{/if}
