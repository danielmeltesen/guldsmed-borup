{*

# Description
Template for Newsletter in a column box


## Date last modified
2014-05-19


## Primary variables
+ $text                                                     - Global scope variable containing translations


## Partials (templates)
No extra templates required for this template

*}

<div class="panel panel-border column-box b-newsletter">
	<form id="b-newsletter-assign" method="post" action="/actions/newsletter/regmail">
        <input type="hidden" name="form" value="quick">
        <input type="hidden" name="type" value="1">

	    <div class="panel-heading b-newsletter-assign-header b-header">
	        <span class="h5">{$text.NEWLSLETTER_MENU}</span>
	    </div>
	    <div class="panel-body">
			<fieldset class="form-group b-newsletter-username">
				<input name="name" type="text" class="form-input small" placeholder="{$text.NAME}" required>
			</fieldset>

			{if $settings.news_signup_type == 'email' || $settings.news_signup_type == 'both'}
			<fieldset class="form-group b-newsletter-email">
				<input name="email" type="text" class="form-input small" placeholder="{$text.MAIL}" required>
			</fieldset>
			{/if}

			{if $settings.news_signup_type == 'sms' || $settings.news_signup_type == 'both'}
			<fieldset class="form-group b-newsletter-mobile">
				<input name="mobile" type="text" class="form-input small" placeholder="{$text.MOBILE}" required>
			</fieldset>
			{/if}
	    </div>
	    <div class="panel-footer">
	        <button class="form-submit button small" type="submit">{$text.CONFIRM}</button>
	    </div>
    </form>
</div>
