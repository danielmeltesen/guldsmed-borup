{*

# Description
Template for Calendar in a column box, the template is build with [AngularJS](https://angularjs.org/).


## Date last modified
2014-12-01


## Resource
+ [AngularJS](https://angularjs.org/)


## Primary variables
+ $text                                                     			- Global scope variable containing translations


## Partials (templates)
No extra templates required for this template

*}

<div class="panel panel-border column-box b-calendar" data-ng-controller="clndr-ctrl">
	<div class="panel-heading b-calendar-header b-header">
		<span class="h5">{$text.CALENDAR_BOX_TITLE}</span>
	</div>
	{if !$client.isCrawler}
		{literal}
		<div class="m-clndr t-clean b-calendar-content ng-cloak" data-ng-cloak data-clndr="clndr">
			<div class="clndr-controls">
				<div class="clndr-previous-button" data-ng-click="clndr.back()">
					<i class="fa fa-fw fa-angle-left"></i>
				</div>
				<div class="month" data-ng-bind-html="month + ' ' + year"></div>
				<div class="clndr-next-button" data-ng-click="clndr.forward()">
					<i class="fa fa-fw fa-angle-right"></i>
				</div>
			</div>
			<div data-ng-class="{'show-events': active}" class="clndr-grid">
				<div class="days-of-the-week">
					<div class="header-day" data-ng-repeat="day in daysOfTheWeek track by $index" data-ng-bind-html="day"></div>
				</div>
				<div class="days" data-ng-click="active = true">
					<div data-ng-class="day.classes" data-ng-repeat="day in days">{{day.day}}</div>
				</div>
				<div class="events">
					<div class="headers">
						<div class="x-button" data-ng-click="active = false"><i class="fa fa-fw fa-angle-left"></i></div>
						<div class="event-header">EVENTS</div>
					</div>
					<div class="content">
					    <div class="event" data-ng-repeat="event in eventsThisMonth">
					    	<a data-ng-href="{{event.Handle}}">{{event.DateStart | moment:'Do MMMM'}}: <span data-ng-bind-html="event.Title"></span></a>
					    </div>
				    </div>
				</div>
			</div>
		</div>
		{/literal}
	{/if}
</div>
