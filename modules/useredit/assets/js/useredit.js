;(function($) {
	'use strict';

	function init () {
		var $wrapper = $("#m-useredit");

		if ($wrapper.length > 0) {
			// Execute form action on select change
			$('#m-useredit-country', $wrapper).on('change', function(e){

				$('.state-group', $wrapper).addClass('is-hidden');
				$('.state-select', $wrapper).each(function() {
					$(this).val('');
				});

				var el = $('#m-useredit-country').find(':selected');
				if (el.data('hasStates') > 0) {
					$('.state-group-' + el.val(), $wrapper).removeClass('is-hidden');
				}
			});

			// City/county locator
			$('#m-useredit-zipcode', $wrapper).SmartForm('county', {target:'#m-useredit-city'});

			// Country code locator
			$('#m-useredit-country', $wrapper).SmartForm('countrycode', {
				target: function(code) {
					$('input.countryCode', $wrapper).val(code);
					$('span.countryCode', $wrapper).html('+' + code);
				}
			});

			// Interest group selector
			$('#m-useredit-newsletter', $wrapper).on('change', function(e) {
				$('#m-useredit-newsletterfields', $wrapper).toggleClass('is-hidden');
			});
		}
	}


	$(function() {
		init();
	});

})(jQuery, window);