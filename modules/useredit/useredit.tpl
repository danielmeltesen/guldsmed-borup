{*

# Description
Controller and template for the User Edit page type


## Date last modified
2015-04-24


## Primary variables
+ $item 																- Entity of userController (the current user)
+ $deliveryCountries 													- Collection of deliveryCountryController
+ $interestFields 														- Collection of interestFieldController
+ $page                                                                 - Global scope variable containing all information about the page type
+ $settings                                                             - Global scope variable containing platform settings
+ $text                                                                 - Global scope variable containing translations


## Partials (templates)
No extra templates required for this template

*}

{* Fetch delivery countrys *}
{collection controller=deliveryCountry assign=deliveryCountries}

{* Fetch interest fields *}
{collection controller=interestField assign=interestFields categoryId=$user->CategoryId}

{* Phone code *}
{$phonecode = $user->CountryCode}

{if $phonecode == 0}
	{$phonecode = $general.deliveryCountryCode}
{/if}

<div class="modules m-useredit" itemscope itemtype="http://schema.org/Article">
	<article class="m-useredit-article">
		<header class="m-useredit-header page-title">
			<h1 class="m-useredit-headline" itemprop="headline">{$text.USER_UPDATE_HEADLINE}</h1>
		</header>

		<div class="m-useredit-description description trailing">
			<p class="m-useredit-description" itemprop="description">{$text.USER_UPDATE_PROFILE_TEXT}. {$text.USER_ADD_REQUIRED_FIELDS}.</p>
		</div>

		<form id="m-useredit" method="post" action="/actions/user/edit">
			<div class="panel panel-border">
    			<div class="panel-body">
    				<div class="row">
    					<div class="col-s-4 col-m-6 col-l-6 col-xl-12">

    						<fieldset class="form-group m-useredit-firstname">
					        	<label class="form-label" for="m-useredit-firstname">{$text.FIRSTNAME} <span class="form-required">*</span></label>
								<input id="m-useredit-firstname" name="firstname" type="text" class="form-input small" placeholder="{$text.FIRSTNAME}" value="{$user->Firstname}" required>
							</fieldset>

							<fieldset class="form-group m-useredit-lastname">
					        	<label class="form-label" for="m-useredit-lastname">{$text.LASTNAME} <span class="form-required">*</span></label>
								<input id="m-useredit-lastname" name="lastname" type="text" class="form-input small" placeholder="{$text.LASTNAME}" value="{$user->Lastname}" required>
							</fieldset>

							{if $settings.shop_customer_company}
							<fieldset class="form-group m-useredit-company">
					        	<label class="form-label" for="m-useredit-company">{$text.COMPANY} {if $user->CategoryId == 4}<span class="form-required">*</span>{/if}</label>
								<input id="m-useredit-company" name="company" type="text" class="form-input small" placeholder="{$text.COMPANY}" value="{$user->Company}"{if $user->CategoryId == 4} required{/if}>
							</fieldset>
							{/if}

							{if $settings.shop_customer_company}
							<fieldset class="form-group m-useredit-vatnumber">
					        	<label class="form-label" for="m-useredit-vatnumber">{$text.VAT_NR} {if $user->CategoryId == 4}<span class="form-required">(*)</span>{/if}</label>
								<input id="m-useredit-vatnumber" name="vatNumber" type="text" class="form-input small" placeholder="{$text.VAT_NR}" value="{$user->VatNumber}">
							</fieldset>
							{/if}

							{if $settings.shop_customer_institution}
							<fieldset class="form-group m-useredit-eannumber">
					        	<label class="form-label" for="m-useredit-eannumber">{$text.EAN} {if $user->CategoryId == 4}<span class="form-required">(*)</span>{/if}</label>
								<input id="m-useredit-eannumber" name="eanNumber" type="text" class="form-input small" placeholder="{$text.EAN}" value="{$user->Ean}">
							</fieldset>
							{/if}

							{if $settings.shop_customer_birthdate}
							<fieldset class="form-group m-useredit-birthday">
					        	<label class="form-label" for="m-useredit-birthday">{$text.BIRTHDATE}</label>
					        	<div class="input-group small">
						        	<span class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></span>
									<input id="m-useredit-birthday" name="birthday" type="date" class="form-input small" value="{$user->DateOfBirth|date_format:'%Y-%m-%d'}">
								</div>
							</fieldset>
							{/if}

							<fieldset class="form-group m-useredit-address">
					        	<label class="form-label" for="m-useredit-address">{$text.ADDRESS} <span class="form-required">*</span></label>
								<input id="m-useredit-address" name="address" type="text" class="form-input small" placeholder="{$text.ADDRESS}" value="{$user->Address}" required>
							</fieldset>

							<fieldset class="form-group m-useredit-zipcode">
					        	<label class="form-label" for="m-useredit-zipcode">{$text.POSTCODE} <span class="form-required">*</span></label>
								<input id="m-useredit-zipcode" name="zipcode" type="text" class="form-input small" placeholder="{$text.POSTCODE}" value="{$user->Zipcode}" required>
							</fieldset>

							<fieldset class="form-group m-useredit-city">
					        	<label class="form-label" for="m-useredit-city">{$text.CITY} <span class="form-required">*</span></label>
								<input id="m-useredit-city" name="city" type="text" class="form-input small" placeholder="{$text.CITY}" value="{$user->City}" required>
							</fieldset>

							{if $deliveryCountries->getActualSize() gt 1}
								<fieldset class="form-group m-useredit-country">
					        		<label class="form-label" for="m-useredit-country">{$text.COUNTRY} <span class="form-required">*</span></label>
									<select id="m-useredit-country" class="form-input form-select small" name="country">
										{foreach $deliveryCountries->getData() as $country}
											<option data-has-states="{count($country->CountryStates)}" value="{$country->Iso}" {if $user->Country == $country->Iso}selected{/if}>{$country->Title}</option>
										{/foreach}
									</select>
								</fieldset>
							{else}
								<input type="hidden" name="country" value="{$general.deliveryCountryIso}">
							{/if}

							{foreach $deliveryCountries->getData() as $country}
								{if count($country->CountryStates) gt 0}
									<fieldset id="cuStateWrapper" class="form-group state-group state-group-{$country->Iso} {if $user->Country != $country->Iso}is-hidden{/if}">
						        		<label class="form-label" for="m-useredit-state">{$text.STATE} <span class="form-required">*</span></label>
										<select id="m-useredit-state" class="state-select form-input form-select small" name="state">
											{foreach $country->CountryStates as $state}
												<option value="{$state}" {if $user->State == $state}selected{/if}>{$state}</option>
											{/foreach}
										</select>
									</fieldset>
								{/if}
							{/foreach}

    					</div>
    					<div class="col-s-4 col-m-6 col-l-6 col-xl-12">

    						<fieldset class="form-group m-useredit-email">
					        	<label class="form-label" for="m-useredit-email">{$text.MAIL} <span class="form-required">*</span></label>
								{$user->Email} (<a href="/{$text.USER_UPDATE_EMAIL_LINK}/" title="{$text.EDIT}">{$text.EDIT}</a>)
							</fieldset>

    						<fieldset class="form-group m-useredit-password">
					        	<label class="form-label" for="m-useredit-password">{$text.PASSWORD} <span class="form-required">*</span></label>
								******** (<a href="/{$text.USER_UPDATE_PASSWORD_LINK}/" title="{$text.EDIT}">{$text.EDIT}</a>)
							</fieldset>

							<input class="countryCode" type="hidden" name="countryCode" value="{$phonecode}">

							{if $settings.shop_customer_phone}
							<fieldset class="form-group m-useredit-phone">
					        	<label class="form-label" for="m-useredit-phone">{$text.TELEPHONE} {if $settings.shop_customer_phone_validation}<span class="form-required">*</span>{/if}</label>
							    <div class="input-group small">
							        <span class="input-group-addon countryCode">+{$phonecode}</span>
							        <input id="m-useredit-phone" name="phone" type="text" class="form-input input-group-main" placeholder="{$text.TELEPHONE}" value="{$user->Phone}" {if $settings.shop_customer_phone_validation}required{/if}>
							    </div>
							</fieldset>
							{/if}

							{if $settings.shop_customer_mobile}
							<fieldset class="form-group m-useredit-mobilephone">
					        	<label class="form-label" for="m-useredit-mobilephone">{$text.MOBILE} {if $settings.shop_customer_mobile_validation}<span class="form-required">*</span>{/if}</label>
							    <div class="input-group small">
							        <span class="input-group-addon countryCode">+{$phonecode}</span>
									<input id="m-useredit-mobilephone" name="mobilephone" type="text" class="form-input input-group-main small" placeholder="{$text.MOBILE}" value="{$user->Mobile}" {if $settings.shop_customer_mobile_validation}required{/if}>
								</div>
							</fieldset>
							{/if}

							{if $settings.news_signup}
							<fieldset class="form-group m-useredit-newsletter">
							    <div class="input-group small">
							        <span class="input-group-addon"><input id="m-useredit-newsletter" type="checkbox" name="newsletter" {if $user->Newsletter}checked="checked"{/if}></span>
							        <label for="m-useredit-newsletter" class="form-label input-group-main">{$text.USER_NEWSLETTER}</label>
							    </div>
							</fieldset>
							{/if}

							{if $interestFields->getActualSize() gt 0}
							<fieldset id="m-useredit-newsletterfields" class="form-group {if !$user->Newsletter}is-hidden{/if}">
								<label class="form-label" for="m-useredit-newsletterfields">{$text.USER_NEWSLETTER_CHOOSE_INTEREST_GROUPS}</label>
								<ul class="list-unstyled list-inline m-useredit-newsletterfields-interestgroups">
								{foreach $interestFields->getData() as $interestField}
									<li>
										<label class="m-useredit-newsletterfields{$interestField->Id} form-label" for="m-useredit-newsletterfields{$interestField->Id}">
											<input id="m-useredit-newsletterfields[{$interestField->Id}]" name="interestGroups[{$interestField->Id}]" type="checkbox"
											{if in_array($interestField->Id, $user->InterestFieldIds)} checked="checked"{/if}> 
											<span class="m-useredit-newsletterfields-label">{$interestField->Title}</span>
											{if in_array($interestField->Id, $user->InterestFieldIds)}<input type="hidden" name="interestGroupsOff[{$interestField->Id}]">{/if}
										</label>
									</li>
								{/foreach}
								</ul>
							</fieldset>
							{/if}

						</div>
					</div>
    			</div>
    			<div class="panel-footer">
    				<button name="submit" type="submit" class="button small">{$text.SAVE}</button>
    			</div>
			</div>
		</form>
	</article>
</div>