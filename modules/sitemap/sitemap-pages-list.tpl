{*

# Description
Template partial for the sitemap sitemap overview. Part of the Sitemap page type.


# Date last modified
2015-03-05


## Primary variables
+ $pageController 														- Instance of pageController
+ $pages 																- Collection of pageController
+ $page                                                                 - Global scope variable containing all information about the page type
+ $text                                                                 - Global scope variable containing translations


## Partials (templates)
No extra templates required for this template

*}

{* Page controller *}
{controller type=page assign=pageController}
{collection controller=$pageController assign=pages}

<div class="modules m-sitemap-page" itemscope itemtype="http://schema.org/Article">
	<article class="m-sitemap-page-article">
		<header class="m-sitemap-page-header page-title">
			<h1 class="m-sitemap-page-headline" itemprop="headline">{$text.SITEMAP_HEADLINE}</h1>
		</header>

		<div class="m-sitemap-page-content content">
			<ul class="m-sitemap-page m-links list-unstyled">
				{foreach $pages->getData() as $pageData}
					<li class="m-sitemap-page-item m-links-page"><a{if !empty($item.target)} target="{$item.target}"{/if} href="{if $pageData->Id eq $page.frontpageId}/{else}{$pageData->Handle|formatUrl}{/if}" title="{$pageData->Title}">{$pageData->Title}</a></li>
				{/foreach}
			</ul>
		</div>
	</article>
</div>
