{*

# Description
Controller for the Sitemap page type.


# Date last modified
2014-08-01


## Primary variables
+ $page                                                                 - Global scope variable containing all information about the page type


## Partials (templates)
+ "/modules/widgets/meta/opengraph.tpl"                                 - Social meta data

*}

{*** Global widgets defaults ***}
{$opengraph_image = NULL}
{$opengraph_title = NULL}
{$opengraph_description = NULL}


{if isset($page.paths.1)}

	{if $page.paths.1 == $text.SITEMAP_LINK_CATEGORIES}
		{include file='modules/sitemap/sitemap-categories-list.tpl'}
	{elseif $page.paths.1 == $text.SITEMAP_LINK_PRODUCTS}
		{include file='modules/sitemap/sitemap-products-list.tpl'}
	{elseif $page.paths.1 == $text.SITEMAP_LINK_PAGES}
		{include file='modules/sitemap/sitemap-pages-list.tpl'}
	{/if}

{else}

	{if $general.isShop}
		{include file='modules/sitemap/sitemap-overview.tpl'}
	{else}
		{include file='modules/sitemap/sitemap-pages-list.tpl'}
	{/if}

{/if}

{*** Global widgets ***}
{include file="modules/widgets/meta/opengraph.tpl"
    og=["title" => $opengraph_title, "description" => $opengraph_description, "image" => $opengraph_image]
}