{*

# Description
Payment icons widget. A widget is a small helper template, with some functionality.


## Date last modified
2014-08-01


## Primary variables
+ $icons                                                    - Shop payment icons


## Partials (templates)
No extra templates required for this template

*}

{paymentIcons assign=icons}
{if $icons->getActualSize() gt 0}
    <div class="payment-icons">
    {foreach $icons->getData() as $icon}
        <span class="payments-icon payments-icon-{$icon@index}"><img alt="{$icon->Title}" title="{$icon->Title}" src="{$template.cdn}{$icon->RelativeFile}"></span>
    {/foreach}
    </div>
{/if}

