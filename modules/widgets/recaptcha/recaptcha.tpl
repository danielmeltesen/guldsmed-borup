{*

# Description
reCAPTCHA widget. A widget is a small helper template, with some functionality.
The widget gets a reCAPTCHA form for spam check.


## Date last modified
2014-08-01


## Primary variables
+ $text                                                                 - Global scope variable containing translations


## Resource
+ [Google Recaptcha dokumentation](http://www.google.com/recaptcha/learnmore)



 *}


 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'custom',
    custom_theme_widget: 'recaptcha_widget',
    lang : 'da'
 };
 </script>

<fieldset class="form-group w-recaptcha widget" id="recaptcha_widget" style="display:none">
    <label class="form-label" for="recaptcha_response_field">{$text.SPAM_CHECK} <span class="form-required">*</span> <small>({$text.SPAM_TEXT_NEW})</small></label>
    <div id="recaptcha_image" onclick="Recaptcha.reload()" style="cursor: pointer; max-width: 100%;"></div><br />
    <div class="recaptcha_only_if_incorrect_sol" style="color:red">{$text.SPAM_ERROR}</div>

    <div class="input-group">
        <span onclick="document.getElementById('recaptcha_image').click();" style="cursor:pointer;" class="input-group-addon"><i class="fa fa-rotate-right"></i></span>
        <input type="text" class="form-input input-group-main" id="recaptcha_response_field" name="recaptcha_response_field" placeholder="{$text.SPAM_CHECK}" required>
    </div>
</fieldset>
 <script src="//www.google.com/recaptcha/api/challenge?k=6LdaUcUSAAAAAD6_zELLUYcOnYkrwYkT40LAg8DU"></script>
