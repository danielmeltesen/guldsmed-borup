{*

# Description
Template for Newsletter signup in a widget


## Date last modified
2014-08-01


## Primary variables
+ $text                                                     - Global scope variable containing translations


## Partials (templates)
No extra templates required for this template

*}

<div class="w-newsletter-signup">
	<form method="post" action="/actions/newsletter/regmail">
        <input type="hidden" name="form" value="quick">
        <input type="hidden" name="type" value="1">

	    <div class="panel-heading w-newsletter-form-header w-header">
	        <span class="h6 w-newsletter-form-headline w-title">{$text.NEWLSLETTER_MENU}</span>
	    </div>
	    <div class="panel-body">

			<fieldset class="form-group w-newsletter-name">
				<input name="name" type="text" class="form-input small" placeholder="{$text.NAME}" required>
			</fieldset>

			{if $settings.news_signup_type == 'email' || $settings.news_signup_type == 'both'}
			<fieldset class="form-group w-newsletter-email">
				<input name="email" type="text" class="form-input small" placeholder="{$text.MAIL}" required>
			</fieldset>
			{/if}

			{if $settings.news_signup_type == 'sms' || $settings.news_signup_type == 'both'}
			<fieldset class="form-group w-newsletter-mobile">
				<input name="mobile" type="text" class="form-input small" placeholder="{$text.MOBILE}" required>
			</fieldset>
			{/if}

			{if $settings.spam_check}
			<div class="m-newsletter-spam-check">
				{include file="modules/widgets/recaptcha/recaptcha.tpl"}
			</div>
			{/if}
	    </div>
	    <div class="panel-footer">
	        <button class="form-submit button small" type="submit">{$text.CONFIRM}</button>
	    </div>
    </form>
</div>
