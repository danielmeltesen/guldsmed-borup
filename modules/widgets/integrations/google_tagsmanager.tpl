{*

# Description
Google Tagsmanager widget. A widget is a small helper template, with some functionality.


## Date last modified
2014-08-01


## Primary variables
+ $settings                                                             - Global scope variable containing platform settings
+ $page                                                                 - Global scope variable containing all information about the page type


## Partials (templates)
No extra templates required for this template

*}


{if isset($settings.google_tag_manager) and $settings.google_tag_manager eq 'true' and isset($settings.google_tag_manager_id) and !empty($settings.google_tag_manager_id)}

    {if $page.isCheckout and isset($smarty.get.approved)}
    <script>
        {orderTracking orderId=$page.orderId type="tagmanager"}
    </script>
    {/if}

    <noscript><iframe src="//www.googletagmanager.com/ns.html?id={$settings.google_tag_manager_id}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    {literal}
    <script id="google-tagmanager">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    {/literal}
    })(window,document,'script','dataLayer','{$settings.google_tag_manager_id}');</script>

{/if}