{*

# Description
Template for single product in the product lists. Part of the Product page type.


## Date last modified
2015-09-11


## Primary variables
+ $item 													- The instance of the product to be shown
+ $related  												- (Optional) Boolean telling if list consists
+ $controller 												- Instance of productController
+ $product 													- Entity of productController (a single shop product)
+ $priceInterval 											- Entity of productPriceController (price lines for selected product)
+ $reviewcontroller 										- Instance of productReviewController
+ $image 													- Entity of filesController with type product (the first image for a product)
+ $brand 													- Entity of userController (brands are users)


## Partials (templates)
No extra templates required for this template

*}

{if !isset($related)}
	{$related = false}
{/if}

{controller type=product assign=controller}
{entity assign=priceInterval controller=productPrice productId=$product->Id}
{controller assign=reviewcontroller type=productReview primary=true}

{$pagelink = {page id=$product->PageId print=Link}}
{$handle = ProductController::link($product->Id, false, $product)}
{$unitTitle = $controller->getUnitTitle($product->UnitId)}
{$showPrices = !in_array($product->Type, ["giftcard","codegiftcard","discontinued"]) and (!empty($user) or $settings.shop_b2b_hidden_prices === false)}

{if !isset($itemClass)}
	{$itemClass = "col-s-4 col-m-12 col-l-12 col-xl-24"}
{/if}

<div class="productItem {$itemClass} productItem{$product->Id} m-productlist-item m-productlist-item-{$product->Id}">
	<article class="productContent m-productlist-wrap type-row">
		<div class="row">
			<div class="col-s-4 col-m-12 col-l-4 col-xl-8">
				{* Product image wrapper *}
				<figure class="m-productlist-figure image">

					{* Product splashes, news, discount and sold out *}
					{if $settings.show_product_icons}
					<div class="splash m-productlist-splash m-product-splash">

						{* NEW icon if newer than 30 days (2.592.000 seconds) *}
						{if $product->Age lt 2592000}
							<span class="badge badge-succes m-productlist-splash-new m-product-splash-new">{$text.NEWS}</span>
						{/if}

						{* DISCOUNT icon if discount is active *}
						{if $showPrices and $priceInterval and $priceInterval->PriceMin lt $priceInterval->FullPriceMin}
							<span class="badge badge-warning m-productlist-splash-sale m-product-splash-sale">{$text.SALE}</span>
						{/if}

						{* SOLD OUT icon if not in stock *}
						{if $product->Soldout}
							<span class="badge badge-danger m-productlist-splash-soldout m-product-splash-soldout">{$text.SOLD_OUT}</span>
						{/if}
					</div>
					{/if}

					{* Product image *}
					<a href="{$handle}" class="m-productlist-link" title="{$product->Title}">
						{entity assign=image controller=files type=shop productId=$product->Id}
						{controller type=files assign=imageController}
						{if $image}
							{$crop = "fill"}
                            {if !empty($template.settings.DESIGN_IMAGE_BACKGROUND_COLOR)}
                                {$crop = $template.settings.DESIGN_IMAGE_BACKGROUND_COLOR}
                            {/if}
							{$imgPath = $image->thumbnail(293, 293, $crop)}
						{else}
							{placeholdImage assign=placeholder width=293 height=293 background=$template.settings.DESIGN_IMAGE_BACKGROUND_COLOR color=$template.settings.FONT_COLOR_PRIMARY text=$text.IMAGE_PLACEHOLDER_TEXT}
							{$imgPath = $placeholder->getRelativeFile()}
						{/if}

						{$imageAlt = $imageController->getTranslation($image->Id, "title")}
						<img class="responsive m-productlist-image" src="{$imgPath|solutionPath}" alt="{if empty($imageAlt)}{$product->Title}{else}{$imageAlt}{/if}">
					</a>
				</figure>
			</div>

			<div class="col-s-4 col-m-12 col-l-4 col-xl-8">
				{* Title of Product *}
				<header class="m-productlist-heading">
					<a href="{$handle}" class="m-productlist-link">
						<h4 class="h4 m-productlist-title">{$product->Title}</h4>
					</a>
				</header>

				{* Rating visually made with FontAwesome *}
				{if $settings.module_shop_review_products}
					{$avgRating = $reviewcontroller->getAverageRating($product->Id)}
				    {collection assign=reviews controller=$reviewcontroller productId=$product->Id}
					{if $avgRating gt 0}
					<div class="m-reviews-stars">
						{* Rating loop *}
						{section name=rating loop=6 start=1}
							<i class="fa fa-star{if $smarty.section.rating.index > $avgRating}-o{/if}"></i>
						{/section}
					</div>
					{/if}
				{/if}

				{* Brand of Product *}
				{entity assign=brand controller=user id=$product->ProducerId}
				{if $brand and !empty($brand->Title)}
				<span class="m-productlist-brand">{$brand->Title}</span>
				{/if}

				{* Model *}
				{if $settings.shop_product_number and $product->ItemNumber}
					<p class="m-productlist-itemNumber">{$product->ItemNumber}</p>
				{/if}

				{* Stock status *}
				{if $settings.shop_product_delivery_time and $settings.shop_product_delivery_time_list}
					<p class="m-productlist-stock">
						{$stock = $product->Stock}
						{$minamount = $product->MinAmount}

						{if $product->DeliveryTimeId <= 0}
							{if $product->Soldout}
								<i class="fa fa-fw fa-clock-o"></i>
								{$text.PRODUCT_CATALOG_STOCK_NOT_HOME}
							{else}
								<i class="fa fa-fw fa-check"></i>
								{$text.PRODUCT_CATALOG_STOCK_HOME}
								{if $product->DeliveryTimeId == 0}
									({$product->Stock} {$unitTitle})
								{/if}
							{/if}
						{else}
							{if $product->Soldout}
								<i class="fa fa-fw fa-clock-o"></i>
								{$controller->getDeliveryTime($product->DeliveryTimeId, false)}
							{else}
								<i class="fa fa-fw fa-check"></i>
								{$controller->getDeliveryTime($product->DeliveryTimeId, true)}
							{/if}
						{/if}
					</p>
				{/if}

				{* Short description *}
				{$description = $controller->getDescriptionList($product->Id)}
				{if $description}
					<div class="m-productlist-description trailing">{$description}</div>
				{/if}

				<hr role="separator">
			</div>

			<div class="col-s-4 col-m-12 col-l-4 col-xl-8 m-productlist-offer">
				<div class="panel panel-border">
					<div class="panel-body">
						<div class="product-transaction">
							{if $showPrices}
								{if $product->CallForPrice}
									<p><span class="h3">{$text.PRODUCT_CATALOG_CALL_FOR_PRICE}</span></p>
								{else}
									{* Price calculation of Product *}
									<div>
										
										{* Discount price *}
										{if $priceInterval->PriceMin lt $priceInterval->FullPriceMin}
								    	<p>
								    		{* Price before discount *}
								    		<s>
								    			{$priceInterval->FullPriceMin|formatPrice}
									    		{* Pieces *}
									    		{if $priceInterval->Amount gt 1}
									    			{$text.OF} {$priceInterval->Amount} {$unitTitle}
									    		{/if}
								    		</s>
								    	</p>
								    	{/if}

								    	{* Price *}
								    	<p>
								    		<span class="h3">
								    			{* Price from *}
								    			{if $priceInterval->PriceMin != $priceInterval->PriceMax}
								    				{$text.PRODUCT_CATALOG_PRICE_FROM}
								    			{/if}

								    			<span class="m-productlist-price">{$priceInterval->PriceMin|formatPrice}</span>

									    		{* Pieces *}
									    		{if $priceInterval->Amount gt 1}
									    			{$text.OF} {$priceInterval->Amount} {$unitTitle}
									    		{/if}
								    		</span>
								    	</p>

							    		{* VAT *}
							    		{if $settings.shop_product_tax_after_price}
							    			<span class="is-block">
							    				{if $currency.hasVat}
							    					{$text.PRODUCT_CATALOG_PRODUCT_PRICE_TAX}
							    				{else}
							    					{$text.PRODUCT_CATALOG_PRODUCT_PRICE_TAX_NO}
							    				{/if}
							    			</span>
							    		{/if}
									</div>
								{/if}
							{/if}

							{* Call-to-action *}
							<fieldset class="form-group">
								<a href="{$handle}" class="button-primary is-block">{$text.PRODUCT_CATALOG_PRODUCT_SHOW}</a>
							</fieldset>
						</div>
					</div>
	        	</div>
			</div>
		</div>
	</article>
</div>
