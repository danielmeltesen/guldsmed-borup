;(function ($, eksport, undefined) {
    'use strict';

    // ==========================================================================
    // Product Prototype
    // ==========================================================================

    var Product = function(id) {
        var self = this;
        this.id = id;
        this.tries = 0;

        this.API = {
            product : "/json/products/id/{productId}"
        };


        this.selectors = {
            unitTitle       : "[data-controller='unittitle']",
            input           : "[data-controller='input']",
            amount          : "[data-controller='amount']",
            panel           : "[data-controller='infoPanel']",
            tmpl            : "#entity-js-infopanel"
        }


        // Get used elements from DOM
        this.$input         = $(this.selectors.input);
        this.$amount        = $(this.selectors.amount);
        this.$panel         = $(this.selectors.panel);
        this.$unitTitle     = $(this.selectors.unitTitle);


        // Save original data
        this.originalUnitTitle  = this.$unitTitle.html();
        this.originalPanel      = this.$panel.html();
    };


    Product.prototype = {
        get : function(cb) {

            if (!this.id) {
                return;
            }

            var self = this;
            self.tries = self.tries + 1;

            $.get(this.API.product.replace("{productId}", this.id))
                .success(function( data ) {
                    self.item = (data.amount && data.products) ? data.products[0] : undefined;
                    $.extend(self, self.item);
                    cb( self.item );
                })
                .fail(function () {
                    if (self.tries < 3) {
                        self.get(self.id);
                    } else {
                        self.item = undefined;
                        cb( self.item );
                    }
                });
        },




        showWarning : function(stop) {
            var self = this,
                panel = this.$panel.find(".panel");

            if ( panel.length ) {
                panel
                    .css({
                        "-webkit-transition": "opacity 0.24s ease-in-out",
                        "-moz-transition": "opacity 0.24s ease-in-out",
                        "transition": "opacity 0.24s ease-in-out"
                    })
                    .removeClass("panel-warning").addClass("panel-danger");

                setTimeout( function () {
                    panel.removeClass("panel-danger").addClass("panel-warning");

                    if (!stop) {
                        self.showWarning(true);
                    }
                }, 600);
            }
        },




        updateUnitTitle : function (variant, reset) {
            var self = this;

            if (reset) {
                self.$unitTitle.html(self.originalUnitTitle);
            } else {
                self.$unitTitle.html(variant.UnitTitle);
            }
        },




        updatePanel : function (variant, reset) {
            var self = this,
                data = {},
                helpers = {};

            if (!self.$panel.length) {
                return;
            }

            if (reset) {
                self.$panel.html(self.originalPanel);
                self.$panel.removeClass("open");
                return;
            }

            data.product = self;
            data.selected = variant;

            helpers.hidePrice = !(window.platform.user || !window.platform.getSetting("shop_b2b_hidden_prices") || !window.platform.getSetting("shop_b2b_hidden_prices") == "partly");
            helpers.hasVat = window.platform.currency.hasVat;
            helpers.showPrice = (!helpers.hidePrice && !!variant.Price && variant.Price.PriceMin !== "undefined");
            helpers.staticdomain = window.platform.template.cdn;

            var infoPanelTmpl = $.templates(this.selectors.tmpl);
            var html = infoPanelTmpl.render(data, helpers);

            self.$panel.html(html);
            self.$panel.addClass("open");
        },





        updatePacketPanel : function (item, reset) {
            var self = this,
                data = {},
                helpers = {},
                html = "";

            if (!self.$panel.length) {
                return;
            }

            if (reset) {
                self.$panel.html(self.originalPanel);
                self.$panel.removeClass("open");
                return;
            }

            if (window.platform.getSetting("shop_product_delivery_time")) {
                data.selected = $.extend({}, item);
                html = $.templates("#entity-js-stock").render(data, helpers);
            } else {
                var tmpl = $.templates('#entity-packet-selected');

                if (!tmpl) {
                    self.$panel.remove();
                    return;
                }

                html = tmpl.render(data, helpers);
            }

            self.$panel.html(html).addClass("open");
            
        },




        updateInputs : function (item, reset) {
            var self = this;

            if (reset) {
                self.$input.length && self.$input.val(self.$input.prop("defaultValue"));
                self.$amount.length && self.$amount.val(self.$amount.prop("defaultValue"));
            } else {
                self.$input.length && self.$input.val(item.Id);
                self.$amount.length && self.$amount.val(item.MinAmount);
            }
        },



        updateImage : function (variant) {
            var self = this,
                fileId = variant.FileId,
                $img = $("img[data-fileid='"+fileId+"']");

            if (window.platform.getSetting("shop_product_image_structure") === "zoom") {
                $img.trigger("click");
            }

            if (window.platform.getSetting("shop_product_image_structure") === "rotation") {
                var idx = $img.parent("li").index();
                $(".owl-carousel.product-slider-theme").data("owlCarousel").goTo(idx);
            }
        }
    };







    eksport.platform = eksport.platform || {};
    eksport.platform.classes = eksport.platform.classes || {};
    eksport.platform.classes.Product = Product;
})(jQuery, window);
