{*

# Description
Template partial for the imageelement of the Product. Part of the Product page type.


## Date last modified
2014-09-05


## Resource
+ [CloudZoom][http://www.starplugins.com/cloudzoom/quickstart]
+ [Thumbelina][http://www.starplugins.com/thumbelina]
+ [Fancybox][http://fancyapps.com/fancybox/#docs]


## Primary variables
+ $partialSettings                                                      - Variable with settings for the product image
+ $id                                                                   - Id of the current product
+ $defaultImage                                                         - Image to be shown if none are available
+ $alt                                                                  - Alt-text for images
+ $settings                                                             - Global scope variable containing platform settings
+ $imageController                                                      - Instance of fileController of type product
+ $items                                                                - Collection of fileController (all product images)
+ $first                                                                - Entity of fileController (first product image)


## Partials (templates)
No extra templates required for this template

*}


{*** Fetch the filesController and save it for later ***}
{controller type=files assign=imageController}

{*** Get instance of the imageController ***}
{collection assign=items controller=$imageController type=product productId=$item->Id}


{if $items->getActualSize() gt 0}
    {*** Setup height and width ***}
    {$itemWidth=null}
    {$itemHeight=null}

    {*** Adjust image, lens, zoom and thumb size***}
    {if isset($partialSettings) && isset($partialSettings.width)}
        {$itemWidth=$partialSettings.width}
        {$partialSettings.lensWidth = "auto"}
        {$partialSettings.zoomWidth = "auto"}
    {/if}

    {if isset($partialSettings) && isset($partialSettings.height)}
        {$itemHeight=$partialSettings.height}
        {$partialSettings.lensHeight = $partialSettings.height|cat:"px"}
        {$partialSettings.zoomHeight = $partialSettings.height|cat:"px"}
    {/if}

    {if isset($partialSettings) && isset($partialSettings.thumbWidth)}
        {$thumbWidth = $partialSettings.thumbWidth}
    {else}
        {$thumbWidth = 80}
    {/if}

    {if isset($partialSettings) && isset($partialSettings.thumbWidth)}
        {$thumbHeight = $partialSettings.thumbHeight}
    {else}
        {$thumbHeight = 80}
    {/if}

    {$zoomImageWidth = null}
    {$zoomImageHeight = null}
    {if isset($partialSettings) && isset($partialSettings.height) && isset($partialSettings.width) && isset($partialSettings.startMagnification)}
        {$zoomImageWidth = $partialSettings.width * 2}
        {$zoomImageHeight = $partialSettings.height * 2}
    {else}
        {$zoomImageWidth = 1220}
        {$zoomImageHeight = 1220}
    {/if}

    {$crop = "fill"}
    {if isset($partialSettings) && isset($partialSettings.crop)}
        {$crop = $partialSettings.crop}
    {elseif !empty($template.settings.DESIGN_IMAGE_BACKGROUND_COLOR)}
        {$crop = $template.settings.DESIGN_IMAGE_BACKGROUND_COLOR}
    {/if}


    {***  Use Id to associate the element (thru 'useZoom') to specific Cloud Zoom instance. ***}
    {if isset($partialSettings) && isset($partialSettings.id)}
        {$partialSettings.useZoom = $partialSettings.id}
        {$zoomId = $partialSettings.id}
        {$sliderId = $partialSettings.id}
    {else}
        {$partialSettings.useZoom = "#cloud-{$product->Id}"}
        {$zoomId = "cloud-{$product->Id}"}
        {$sliderId = $product->Id}
    {/if}


    {*** Build the ZoomSlider thingy ***}
    <div id="template-myCloudZoom-{$sliderId}" class="product-image">
        {*** Get information for the first image ***}
        {entity assign=first controller=$imageController type=product productId=$item->Id}
        {$partialSettings.zoomImage = $first->thumbnail($zoomImageWidth, $zoomImageHeight, $crop, $template.watermark)|solutionPath}
        {$fancyImage = $first->thumbnail(null, null, false, $template.watermark)|solutionPath}

        {*** Setup rest of items properties ***}
        {$itemTitle = $imageController->getTranslation($first->Id, "title")}
        {$itemText = $imageController->getTranslation($first->Id, "text")}

        <div>
            <img class="responsive cloudzoom"
                 itemprop="image"
                 data-cloudzoom='{json_encode($partialSettings)}'
                 data-index="0"
                 src="{$first->thumbnail($itemWidth, $itemHeight, $crop, $template.watermark)|solutionPath}"
                 alt="{if $itemTitle == "" }{$alt}{else}{$itemTitle}{/if}"
                 id="{$zoomId}">

            {$fancyArray = [
                'href' => $fancyImage,
                'title'=> "{if empty($itemTitle) }{$alt|utf8_encode}{else}{$itemTitle|utf8_encode}{/if}",
                'type' => 'image'
            ]}
        </div>

        {*** Only show image changer, if there is actually more then one picture ***}
        {if $items->getActualSize() gt 1}
            <div id="slider-{$sliderId}" class="thumbelina-wrapper" style="display:none;position:relative;height: {$thumbHeight+2|cat:"px"};">
                <div  class="thumbelina-but horiz left"><i class="fa fa-fw fa-caret-left"></i></div>
                    <ul>

                    {$fancyArray = []}

                    {*** Run thru all images and list for zoomslider to use ***}
                    {foreach $items->getData() as $item}
                        {*** Define width and height used in thumbnail ***}
                        {$itemSrc = $item->thumbnail($itemWidth, $itemHeight, $crop, $template.watermark)|solutionPath}
                        {$fancyImage = $item->thumbnail(null, null, false, $template.watermark)|solutionPath}
                        {$thumbSrc = $item->thumbnail($thumbWidth, $thumbHeight, true)|solutionPath}
                        {*** Setup rest of items properties ***}
                        {$itemTitle = $imageController->getTranslation($item->Id, "title")}
                        {$itemText = $imageController->getTranslation($item->Id, "text")}

                        {$itemData = [
                            "image"                 =>  $itemSrc,
                            "useZoom"               =>  $partialSettings.useZoom,
                            "zoomImage"             =>  $item->thumbnail($zoomImageWidth, $zoomImageHeight, $crop, $template.watermark)|solutionPath
                        ]}

                        {*** Slider item image ***}
                        <li>
                            <img class="cloudzoom-gallery"
                                 itemprop="image"
                                 data-cloudzoom='{json_encode($itemData)}'
                                 data-fileid="{$item->Id}"
                                 data-index="{$item@index}"
                                 src="{$thumbSrc}"
                                 alt="{if empty($itemTitle) }{$alt}{else}{$itemTitle}{/if}">
                        </li>

                        {$fancyArray[] = [
                            'href' => $fancyImage,
                            'title'=> "{if empty($itemTitle) }{$alt|utf8_encode}{else}{$itemTitle|utf8_encode}{/if}",
                            'type' => 'image'
                        ]}
                    {/foreach}
                    </ul>

                <div class="thumbelina-but horiz right"><i class="fa fa-fw fa-caret-right"></i></div>
            </div>
        {/if}
    </div>

    {strip}
    <script type="text/javascript">
    ;(function (){
        var images = {$fancyArray|json_encode};
        window.onload = function(){
            CloudZoom.quickStart();

            {*** Only activate image changer, if there is actually more then one picture ***}
            {if $items->getActualSize() gt 1}
            var $slider = $('#slider-{$sliderId}');
            $slider.Thumbelina({
                $bwdBut:$slider.find('.left'),
                $fwdBut:$slider.find('.right')
            });
            $slider.delay(400).show(200);
            {/if}

            {***  Bind a click event to a Cloud Zoom instance. ***}
            $('#{$zoomId}').on('click', function(){
                var cloudZoom = $(this).data('CloudZoom'),
                    idx = $(".cloudzoom-gallery").index( $(".cloudzoom-gallery-active") );
                idx = idx === -1 ? 0 : idx;

                cloudZoom.closeZoom();
                {literal}
                $.fancybox.open(images, $.extend({index: idx}, window.platform.fancybox.settingsDefault));
                {/literal}
                return false;
            });
        };
    })();
    </script>
    {/strip}
{elseif isset($defaultImage)}
    <div class="product-image">
        <img class="responsive" src="{$defaultImage}" alt="">
    </div>
{/if}

