{*

# Description
AngularJS template for Javascript filtering of products. Part of the Product page type.


## Date last modified
2015-08-13


## Resource
+ [AngularJS](https://angularjs.org/)


## Primary variables
+ filterSelected                                            - AngularJS model with all selected filter data (price, brand, category ...)
+ text                                                      - Javascript version of Smarty $text with an array of translations


## Partials (templates)
No extra templates required for this template

*}

{literal}
<div class="productfilter panel panel-border ng-hide is-hidden-print" data-ng-show="showFilter" data-ng-element-ready="triggerEvent('filterloaded')">
    <div class="panel-heading">
        <div class="row">
            <div class="col-s-3 col-m-8 col-l-8 col-xl-16">
                <span class="h5" data-ng-bind-html="::text.PRODUCT_CATALOG_FILTER"></span>
            </div>
            <div class="col-s-1 col-m-4 col-l-4 col-xl-8">
                <button type="submit" class="button xsmall pull-right" data-toggle="collapse" data-class="is-collapsed" data-target="panel-filter"><i class="fa fa-angle-double-up icon-close"></i><i class="fa fa-angle-double-down icon-open"></i></button>
            </div>
        </div>
    </div>
    <div class="panel-body" data-group="panel-filter">
        <div class="row">
            <div class="col-s-4 col-m-6 col-l-12 col-xl-12 m-filter-search" data-ng-show="filterMap.search.show">
                <!-- Free range search -->
                <label class="form-label" data-ng-bind-html="::text.SEARCH_LONG"></label>
                <fieldset class="form-group">
                    <input class="form-input small" type="text" placeholder="{{::searchText}}" data-ng-model="filterSelected.search" data-ng-change="searchChanged = true">
                </fieldset>
            </div>
            <div class="col-s-4 col-m-6 col-l-12 col-xl-12 m-filter-price" data-ng-show="::filterMap.prices && filterMap.prices.show && (filterMap.prices.data.low || filterMap.prices.data.high)">
            	<!-- Price range -->
                <label class="form-label" data-ng-bind-html="::text.PRODUCT_CATALOG_FILTER_PRICE"></label>
                <rzslider rz-slider-floor="filterMap.prices.data.low" rz-slider-ceil="filterMap.prices.data.high" rz-slider-model="filterSelected.minPrice" rz-slider-high="filterSelected.maxPrice" rz-slider-translate="currency_format"></rzslider>
            </div>
        </div>

    	<!-- Brands and secondary categories -->
    	<div class="row">

    		<!-- Brands -->
    		<div class="col-s-4 col-m-12 col-l-6 col-xl-12" data-ng-show="::filterMap.brands.show && filterMap.brands.data.length > 0" data-ng-switch data-on="filterMap.brands.show">
                <label class="form-label" data-ng-bind-html="::text.MANUFACTUERER"></label>
                <div data-ng-switch-default>
                    <fieldset class="form-group" data-ng-repeat="data in filterMap.brands.data | orderBy:'title' track by data.id">
                        <label class="input-group small">
                            <span class="input-group-addon">
                                <input type="checkbox" checklist-model="filterSelected.brand" checklist-value="data.id" data-ng-disabled="data.disabled" checklist-change="state('brands')">
                            </span>
                            <span class="form-label input-group-main" data-ng-class="{'is-disabled':data.disabled}" data-ng-bind-html="data.title"></span>
                        </label>
                    </fieldset>
                </div>

                <div data-ng-switch-when="select">
                    <fieldset class="form-group">
                        <select class="form-input form-select small" data-ng-model="filterSelected.brand" data-ng-options="data.id as data.title disable when data.disabled for data in filterMap.brands.data | orderBy:'title'" data-ng-change="state('brands')">
            				<option value data-ng-bind-html="::text.CHOOSE"></option>
            			</select>
                    </fieldset>
                </div>
    		</div>

    		<!-- Secondary categories -->
    		<div class="col-s-4 col-m-12 col-l-6 col-xl-12" data-ng-show="::filterMap.categories.show && filterMap.categories.data.length > 0" data-ng-switch data-on="filterMap.categories.show">
                <label class="form-label" data-ng-bind-html="::text.CATEGORYS"></label>
                <div data-ng-switch-default>
                    <fieldset class="form-group" data-ng-repeat="data in filterMap.categories.data | orderBy:'title' track by data.id">
                        <label class="input-group small">
                            <span class="input-group-addon">
                                <input type="checkbox" checklist-model="filterSelected.categories" checklist-value="data.id" data-ng-disabled="data.disabled" checklist-change="state('categories')">
                            </span>
                            <span class="form-label input-group-main" data-ng-class="{'is-disabled':data.disabled}" data-ng-bind-html="data.title"></span>
                        </label>
                    </fieldset>
                </div>

                <div data-ng-switch-when="select">
                    <fieldset class="form-group">
                        <select class="form-input form-select small" data-ng-model="filterSelected.categories" data-ng-options="data.id as data.title disable when data.disabled for data in filterMap.categories.data | orderBy:'title'" data-ng-change="state('categories')">
                            <option value data-ng-bind-html="::text.CHOOSE"></option>
                        </select>
                    </fieldset>
                </div>
    		</div>
    	</div>

    	<!-- Variants and custom data -->
    	<div class="row">
			<div class="col-s-4 col-m-6 col-l-6 col-xl-12" data-ng-repeat="dataGroup in filterMap.data | orderBy:['sorting','title'] track by dataGroup.id" data-ng-show="dataGroup.show && (dataGroup.data.length || (dataGroup.show == 'range' && (dataGroup.data.low || dataGroup.data.high)))" data-ng-switch data-on="dataGroup.show">
				<label class="form-label" data-ng-bind-html="dataGroup.title"></label>
				<div data-ng-switch-default>
					<fieldset class="form-group" data-ng-repeat="value in dataGroup.data | orderBy:['sorting','title'] track by value.id">
						<label class="input-group small">
					        <span class="input-group-addon">
					        	<input type="checkbox" checklist-model="filterSelected.data[dataGroup.id]" checklist-value="value.id" data-ng-disabled="value.disabled" checklist-change="state(dataGroup.id)">
					        </span>
					        <span class="form-label input-group-main" data-ng-class="{'is-disabled':value.disabled}" data-ng-bind-html="value.title"></span>
					    </label>
					</fieldset>
				</div>
				<div data-ng-switch-when="select">
                    <fieldset class="form-group">
                        <select class="form-input form-select small" data-ng-model="filterSelected.data[dataGroup.id]" data-ng-options="value.id as value.title disable when value.disabled for value in dataGroup.data | orderBy:['sorting','title']" data-ng-change="state(dataGroup.id)">
                            <option value data-ng-bind-html="::text.CHOOSE"></option>
                        </select>
                    </fieldset>
				</div>
				<div data-ng-switch-when="range">
                    <rzslider rz-slider-floor="dataGroup.data.low" rz-slider-ceil="dataGroup.data.high" rz-slider-model="filterSelected.data[dataGroup.id].from" rz-slider-high="filterSelected.data[dataGroup.id].to" data-group-unit="{{dataGroup.data.unit}}" rz-slider-translate="rzTranslate"></rzslider>
				</div>
			</div>
    	</div>

        <hr role="separator">

        <!-- Toggle news and sale -->
        <div class="row">
            <div class="col-s-4 col-m-12 col-l-6 col-xl-12" data-ng-show="filterMap.new.show">
                <fieldset class="form-group">
                    <label class="input-group small">
                        <span class="input-group-addon">
                            <input type="checkbox" id="filter-shownews" data-ng-model="filterSelected.news" data-ng-change="state('news')" data-ng-disabled="filterMap.new.disabled">
                        </span>
                        <span class="form-label input-group-main" data-ng-class="{'is-disabled':filterMap.new.disabled}" data-ng-bind-html="::text.NEWS"></span>
                    </label>
                </fieldset>
            </div>
            <div class="col-s-4 col-m-12 col-l-6 col-xl-12" data-ng-show="filterMap.sale.show">
                <fieldset class="form-group">
                    <label class="input-group small">
                        <span class="input-group-addon">
                            <input type="checkbox" id="filter-showsale" data-ng-model="filterSelected.sale" data-ng-change="state('sale')" data-ng-disabled="filterMap.sale.disabled">
                        </span>
                        <span class="form-label input-group-main" data-ng-class="{'is-disabled':filterMap.sale.disabled}" data-ng-bind-html="::text.SALE"></span>
                    </label>
                </fieldset>
            </div>
        </div>
    </div>



    <!-- Control buttons -->
    <div class="panel-footer" data-group="panel-filter">
    	<div class="row">
    		<div class="col-s-4 col-m-12 col-l-12 col-xl-24 text-right">
    			<button class="button small" data-ng-show="filterChanged" data-ng-click="clearFilter()" data-ng-bind-html="::text.PRODUCT_CATALOG_FILTER_RESET"></button>
    		</div>
    	</div>
    </div>
</div>
{/literal}
