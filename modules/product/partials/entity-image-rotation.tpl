{*

# Description
Template partial for the imageelement of the Product. Part of the Product page type.


## Date last modified
2014-08-01


## Resource
+ [CloudZoom][http://www.starplugins.com/cloudzoom/quickstart]
+ [Thumbelina][http://www.starplugins.com/thumbelina]
+ [Fancybox][http://fancyapps.com/fancybox/#docs]


## Primary variables
+ $partialSettings                                                      - Variable with settings for the product image
+ $id                                                                   - Id of the current product
+ $defaultImage                                                         - Image to be shown if none are available
+ $alt                                                                  - Alt-text for images
+ $settings                                                             - Global scope variable containing platform settings
+ $imageController                                                      - Instance of fileController of type product
+ $items                                                                - Collection of fileController (all product images)


## Partials (templates)
No extra templates required for this template

*}


{*** Fetch the filesController and save it for later ***}
{controller type=files assign=imageController}

{*** Get instance of the imageController ***}
{collection assign=items controller=$imageController type=product productId=$item->Id}

{if $items->getActualSize() gt 0}
    {*** Setup height and width ***}
    {$itemWidth=null}
    {$itemHeight=null}

    {if isset($partialSettings) && isset($partialSettings.thumbWidth)}
        {$thumbWidth = $partialSettings.thumbWidth}
    {else}
        {$thumbWidth = 80}
    {/if}

    {if isset($partialSettings) && isset($partialSettings.thumbWidth)}
        {$thumbHeight = $partialSettings.thumbHeight}
    {else}
        {$thumbHeight = 80}
    {/if}

    {if isset($partialSettings) && isset($partialSettings.id)}
        {$sliderId = $partialSettings.id}
    {else}
        {$sliderId = $product->Id}
    {/if}

    {$partialSettings["watermark"] = true}

    {*** Build the ZoomSlider thingy ***}
    <div id="template-myRotation-{$sliderId}" class="product-image">

        {*** Include the slider template and send along the settings and information to fetch from the filesController ***}
        {include file="modules/widgets/slider/slider.tpl" widgetSettings=$partialSettings type=product id=$id useFancyBox=true defaultImage=$defaultImage microdata=true alt=$alt}

        {*** Only show image changer, if there is actually more then one picture ***}
        {if $items->getActualSize() gt 1}
            <div id="slider-{$sliderId}" class="thumbelina-wrapper" style="display:none;position:relative;height: {$thumbHeight+2|cat:"px"};">
                <div  class="thumbelina-but horiz left"><i class="fa fa-fw fa-caret-left"></i></div>
                    <ul>
                    {*** Run thru all images and list for zoomslider to use ***}
                    {foreach $items->getData() as $item}
                        {*** Define width and height used in thumbnail ***}
                        {$thumbSrc = $item->thumbnail($thumbWidth, $thumbHeight, true)|solutionPath}
                        {*** Setup rest of items properties ***}
                        {$itemTitle = $imageController->getTranslation($item->Id, "title")}

                        {*** Slider item image ***}
                        <li>
                            <img class="rotation-gallery"
                                 itemprop="image"
                                 data-fileid="{$item->Id}"
                                 src="{$thumbSrc}"
                                 alt="{if $itemTitle == "" }{$alt}{else}{$itemTitle}{/if}">
                        </li>
                    {/foreach}
                    </ul>

                <div class="thumbelina-but horiz right"><i class="fa fa-fw fa-caret-right"></i></div>
            </div>
        {/if}
    </div>

    {if $items->getActualSize() gt 1}
    {strip}
        {*** Only activate image changer, if there is actually more then one picture ***}
        <script type="text/javascript">
        ;(function (){
            window.onload = function(){
                var $slider = $('#slider-{$sliderId}');

                $slider.Thumbelina({
                    $bwdBut:$slider.find('.left'),
                    $fwdBut:$slider.find('.right')
                });

                $slider.delay(400).show(200);
                $slider.find("li").on("click", function (e) {
                    var $this = $(this);
                    $("#template-myRotation-{$sliderId} .owl-carousel").data("owlCarousel").goTo($this.index());
                });
            };
        })();
        </script>
    {/strip}
    {/if}
{elseif isset($defaultImage)}
    <div class="product-image">
        <img class="responsive" src="{$defaultImage}" alt="">
    </div>
{/if}
