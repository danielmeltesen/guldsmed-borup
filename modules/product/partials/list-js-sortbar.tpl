{*

# Description
AngularJS template for Javascript sorting of products. Part of the Product page type.


## Date last modified
2015-08-13


## Resource
+ [AngularJS](https://angularjs.org/)


## Primary variables
+ orderProp                                                 - AngularJS model with order options (price, most bought ...) based on shop settings
+ limitProp                                                 - AngularJS model with limit options (items pr. page) based on shop settings
+ items                                                     - Collection of products based on: category, filter and sorting
+ text                                                      - Javascript version of Smarty $text with an array of translations


## Partials (templates)
No extra templates required for this template

*}

{literal}
<div class="productsortbar panel panel-border product-scroll-target is-hidden-print" data-ng-hide="empty || (orderOptions.length < 2 && (!limitOptions.length || amount < limitOptions[0].amount) && !settings.module_shop_productlist_allow_switch && maxPage < 2)">
	<div class="panel-heading">
        <div class="row">
            <div class="col-s-3 col-m-8 col-l-8 col-xl-16">
                <span class="h5" data-ng-bind-html="::text.PRODUCT_CATALOG_SORTING"></span>
            </div>
            <div class="col-s-1 col-m-4 col-l-4 col-xl-8">
                <button type="submit" class="button xsmall pull-right" data-toggle="collapse" data-class="is-collapsed" data-target="panel-sorting"><i class="fa fa-angle-double-up icon-close"></i><i class="fa fa-angle-double-down icon-open"></i></button>
            </div>
        </div>
    </div>
    <div class="panel-body" data-group="panel-sorting">
    	<div class="row">
    		<div class="col-s-4 col-m-6 col-l-6 col-xl-12">
    			<div class="row">
		    		<div data-ng-show="orderOptions.length > 1" class="col-s-4 col-m-6 col-l-6 col-xl-12">
			        	<label class="form-label" data-ng-bind-html="::text.PRODUCT_CATALOG_ORDER_BY_TEXT"></label>
		        		<fieldset class="form-group">
		        			<select class="form-input form-select xsmall" data-ng-change="changeOrder()" data-ng-model="orderProp" data-ng-options="option.id as option.title for option in orderOptions"></select>
						</fieldset>
					</div>
					<div data-ng-show="limitOptions.length > 1" class="col-s-4 col-m-6 col-l-6 col-xl-12">
			        	<label class="form-label" data-ng-bind-html="::text.PRODUCT_CATALOG_LIMIT_TEXT"></label>
		        		<fieldset class="form-group">
						    <select class="form-input form-select xsmall" data-ng-change="changeLimit()" data-ng-model="limitProp" data-ng-options="limit.amount as limit.title for limit in limitOptions"></select>
						</fieldset>
					</div>
				</div>
			</div>

			<div class="col-s-4 col-m-6 col-l-6 col-xl-12">

				<div class="row">
					<div class="col-s-4 col-m-4 col-l-4 col-xl-8" data-ng-if="settings.module_shop_productlist_allow_switch">
						<label class="form-label" data-ng-bind-html="::text.PRODUCT_CATALOG_VIEW"></label>

						<fieldset class="form-group">
							<div class="input-group xsmall">
								<span class="input-group-button"><button class="button is-block" data-ng-click="changeType(1)"><i class="fa fa-th-list"></i></button></span>
								<span class="input-group-button"><button class="button is-block" data-ng-click="changeType(2)"><i class="fa fa-th"></i></button></span>
							</div>
						</fieldset>
					</div>

					<div class="col-s-4 col-m-4 col-l-4 col-xl-8">
						<label class="form-label" data-ng-bind-html="::text.PRODUCT_CATALOG_PRODUCTS"></label>
						<fieldset class="form-group">
							<span>{{amount}}</span>
						</fieldset>
					</div>

					<div class="col-s-4 col-m-4 col-l-4 col-xl-8" data-ng-hide="maxPage < 2">
						<label class="form-label">{{::text.PAGINATION_PAGE}} {{page}} / {{maxPage}}</label>
				    	<ul class="pagination small">
							<li data-ng-class="{'is-disabled': page == 1}">
								<a href="" data-ng-click="prevPage()"><i class="fa fa-angle-left"></i></a>
							</li>

							<li data-ng-class="{'is-disabled': page == pagedItems.length}">
								<a href="" data-ng-click="nextPage()"><i class="fa fa-angle-right"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>



		</div>
   	</div>
</div>
{/literal}
