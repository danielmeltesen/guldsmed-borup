{*

# Description
Template for a category with the product list. Part of the Product page type.


## Date last modified
2015-08-05


## Primary variables
+ $item														- Entity of categoryController (a single shop product category)
+ $controller												- Instance of productCategoryController
+ $userController											- Instance of userController
+ $brand                                                    - Entity of userController (brands are users)
+ $categories												- Collection of subCategories to the selected category
+ $columns 													- Global scope variable containing columns
+ $text                                                     - Global scope variable containing translations


## Partials (templates)
+ "/modules/product/partials/category-description.tpl"		- Partial template for category description
+ "/modules/product/product-list-combined.tpl" 				- Controller for the category / product list

*}


{$type = 'categoryId'}
{$id = null}
{$title = null}
{$description = null}
{$descriptionBottom = null}

{controller assign=controller type=productCategory}

<div class="modules productlist">

	{* Category description *}
	{$categories = false}
	{if isset($smarty.get.brand)}
		{$type = 'brand'}
		{$id = (int) $smarty.get.brand}
		{controller assign=userController type=user}
		{entity assign=brand controller=$userController id=$id}
		{if $brand}
			{$title = $brand->Title}
			{$description = $userController->getDescription($brand->Id)}
			{$descriptionBottom = $userController->getTranslation($brand->Id, "text_two")}
			{$seoTitle = $userController->getTranslation($brand->Id, 'seo_title')}
			{$seoDescription = $userController->getTranslation($brand->Id, 'seo_description')}

			{if !$seoTitle}
				{$seoTitle = $title}
			{/if}

			{if !$seoDescription}
				{$seoDescription = $description|strip_tags|truncate:160}
			{/if}

			{include file="modules/widgets/meta/opengraph.tpl"
			    og=["title" => $seoTitle, "description" => $seoDescription]
			}
		{else}
			{notfound}
		{/if}
	{elseif isset($smarty.get.new)}
		{$type = 'new'}
		{$title = $text.NEWS}
	{elseif isset($smarty.get.sale)}
		{$type = 'sale'}
		{$title = $text.SALE}
	{elseif isset($smarty.get.search)}
		{$id = $smarty.get.search}
		{$type = 'search'}
		{$title = $text.SEARCH_LONG}
	{elseif isset($smarty.get.tag)}
		{$id = $smarty.get.tag}
		{if isset($smarty.get.type)}
			{$id = {$id|cat:';'|cat:$smarty.get.type}}
		{/if}
		{$type = 'tag'}
		{$title = $text.TAGS}
	{else}
		{$id = $item->Id}
		{$title = $item->Title}
		{$description = $controller->getDescription($item->Id)}
		{$descriptionBottom = $controller->getDescriptionBottom($item->Id)}
		{collection assign=categories controller=productCategory parentId=$id}
	{/if}

	{include file='modules/product/partials/category-description.tpl' title=$title description=$description}

	{$showEmpty = true}
	{if $categories and $categories->getActualSize() gt 0}
		{$showEmpty = false}
		<div class="row">
			{foreach $categories->getData() as $item}
				{include file='modules/product/product-categories-entity.tpl' item=$item}
			{/foreach}
		</div>
	{/if}

	{include file='modules/product/product-list-combined.tpl' type=$type showEmpty=$showEmpty id=$id}

	{if !empty($descriptionBottom)}
		{include file='modules/product/partials/category-description-bottom.tpl' description=$descriptionBottom}
	{/if}
</div>
