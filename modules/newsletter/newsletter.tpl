{*

# Description
Controller for the Newsletter page type


## Date last modified
2014-08-01


## Primary variables
+ $interestFields 														- Fields of interest (like new products, products on sale ...)
+ $text                                                     			- Global scope variable containing translations
+ $page                                                                 - Global scope variable containing all information about the page type


## Partials (templates)
+ "/modules/widgets/recaptcha/recaptcha.tpl"                			- Recaptcha widget, for blog comments
+ "/modules/widgets/meta/meta.tpl"                          			- SEO meta and link data
+ "/modules/widgets/meta/opengraph.tpl"                     			- Social meta data

*}


{*** Global widgets defaults ***}
{$opengraph_image = NULL}
{$opengraph_title = $text.NEWLSLETTER_TEXT}
{$opengraph_description = $text.NEWLSLETTER_HEADLINE}

{*** Meta tag - no noindex,follow ***}
{addMeta name="robots" content="noindex,follow"}

{* Fetch interest fields *}
{collection controller=interestField assign=interestFields categoryId=3}

<div class="modules m-newsletter" itemscope itemtype="http://schema.org/Article">
	<article class="m-newsletter-article">
		<header class="m-newsletter-header page-title">
			<h1 class="m-newsletter-headline" itemprop="headline">{$text.NEWLSLETTER_HEADLINE}</h1>
		</header>

		<div class="m-newsletter-description description trailing">
			<p class="m-newsletter-description" itemprop="description">{$text.NEWLSLETTER_TEXT}</p>
		</div>
		<form id="m-newsletter" method="post" action="/actions/newsletter/regmail">
			<div class="panel panel-border">
    			<div class="panel-body">

					<fieldset class="form-group m-newsletter-name">
			        	<label class="form-label" for="m-newsletter-name">{$text.NAME} <span class="form-required">*</span></label>
						<input id="m-newsletter-name" name="name" type="text" class="form-input small" placeholder="{$text.NAME}" {if $returnPostData.name}value="{$returnPostData.name}"{/if} required>
					</fieldset>

					{if $settings.news_signup_type == 'email' || $settings.news_signup_type == 'both'}
					<fieldset class="form-group m-newsletter-email">
			        	<label class="form-label" for="m-newsletter-email">{$text.MAIL} <span class="form-required">*</span></label>
						<input id="m-newsletter-email" name="email" type="text" class="form-input small" placeholder="{$text.MAIL}" {if $returnPostData.email}value="{$returnPostData.email}"{/if} required>
					</fieldset>
					{/if}

					{if $settings.news_signup_type == 'sms' || $settings.news_signup_type == 'both'}
					<fieldset class="form-group m-newsletter-mobile">
			        	<label class="form-label" for="m-newsletter-mobile">{$text.MOBILE}</label>
						<input id="m-newsletter-mobile" name="mobile" type="text" class="form-input small" placeholder="{$text.MOBILE}" {if $returnPostData.mobile}value="{$returnPostData.mobile}"{/if} required>
					</fieldset>
					{/if}

					<div class="row">
						<div class="col-s-4 col-m-6 col-l-6 col-xl-12">
							<fieldset class="form-group m-newsletter-signup">
							    <div class="input-group small">
							        <span class="input-group-addon"><input id="m-newsletter-signup" type="radio" name="type" value="1" checked></span>
							        <label for="m-newsletter-signup" class="form-label input-group-main">{$text.NEWSLETTER_SIGNIN}</label>
							    </div>
							</fieldset>
						</div>
						<div class="col-s-4 col-m-6 col-l-6 col-xl-12">
							<fieldset class="form-group m-newsletter-signoff">
							    <div class="input-group small">
							        <span class="input-group-addon"><input id="m-newsletter-signoff" type="radio" name="type" value="0"></span>
							        <label for="m-newsletter-signoff" class="form-label input-group-main">{$text.NEWSLETTER_SIGNOFF}</label>
							    </div>
							</fieldset>
						</div>
					</div>

					{if $interestFields->getActualSize() gt 0}
					<fieldset id="m-newsletter-newsletterfields" class="form-group">
						<label class="form-label" for="m-newsletter-newsletterfields">{$text.USER_NEWSLETTER_CHOOSE_INTEREST_GROUPS}</label>
						<ul class="list-unstyled list-inline m-newsletter-newsletterfields-interestgroups">
						{foreach $interestFields->getData() as $interestField}
							<li>
								<label class="m-newsletter-newsletterfields{$interestField->Id} form-label" for="m-newsletter-newsletterfields{$interestField->Id}">
									<input id="m-newsletter-newsletterfields[{$interestField->Id}]" name="interestGroups[{$interestField->Id}]" type="checkbox"
									{if isset($returnPostData.interestGroups) and isset($returnPostData.interestGroups[$interestField->Id])} checked="true"{/if}> 
									<span class="m-newsletter-newsletterfields-label">{$interestField->Title}</span>
								</label>
							</li>
						{/foreach}
						</ul>
					</fieldset>
					{/if}

					{if $settings.spam_check}
					<div class="m-newsletter-spam-check">
						<hr role="separator">
						{include file="modules/widgets/recaptcha/recaptcha.tpl"}
    				</div>
    				{/if}

    			</div>
    			<div class="panel-footer">
    				<button name="submit" type="submit" class="button small">{$text.CONFIRM}</button>
    			</div>
			</div>

		</form>
	</article>
</div>


{*** Global widgets ***}
{include file="modules/widgets/meta/opengraph.tpl"
    og=["title" => $opengraph_title, "description" => $opengraph_description, "image" => $opengraph_image]
}